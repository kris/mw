[1;4mMilliways Scripting language[0m

[4mIntroduction[0m

First of all, in order to understand the scripting format, it would be best
to have a look at the [1mmwrc[0m help, since this explains the layout of the
files themselves. *This* file is only interested in the layout of any single
function.


[4mFunction Layout[0m

A program/function is laid out as follows (for more information on the actual
function header - the 'function' line, see "mwrc" help):

    [1mfunction[0m <name>
        OPCODE args..
        ...

      label:
        OPCODE args..
        ...
    [1mendfunc[0m


To make a function visible for running by the user, it has to be bound to a
command. See the "[1mmwrc[0m" help on how to this.

When a function is run, it is often given arguments. This is done either from
the commandline:

    ,[4mbind[0m [[4marg[0m ...]
    eg:
      ,listusers
      ,slap someuser
      ,do print big text "hello"

or from another function:

    [1mJUMP[0m [4mfunction[0m [[4marg[0m ...]
    [1mCALL[0m [4mvariable[0m [4mfunction[0m [[4marg[0m ...]

    For more information on these two commands, see their respective
    ".scrhelp" context help.

There are several ways to access these arguments from within a function:
These take the form of special variables - their contents being created
automatically when accessed. You *cannot* write to these variable. If you
attempt to do so, the operation fails. These special variables are:

   $0        - The name of the current function
   $1 .. $[4mn[0m  - Each seperate argument passed to the current function
   $#        - The number of arguments passed to the current function
   $*        - The entire argument string of the whole input without parsing
   $%        - Every argument except the first one given together as string
   $^[[4mn[0m]     - Include arguments to this function as arguments to the called
               function. Only worked in [1mJUMP[0m and [1mCALL[0m operations (see
               their respective scrhelp files). If [4mn[0m is missed out, assumes
               [4mn[0m is 2.

               For example:
                 [1mJUMP[0m test $^            - includes $2..$# arguments
                 [1mJUMP[0m test $^4           - includes $4..$# arguments


Because variables start with the letter "$", you can use "$$" in order to get
a single dollar in your scripts.

Each function consits of one or more OPCODE's. There must only be one OPCODE
per line. For a list of OPCODE's, see the talker command ".scrhelp". By
providing the name of the opcode afterwards, you can obtain even more detailed
help.

