
           "It's easier to ask forgiveness than it is to get permission."
                                                          -- Grace Hopper

                                                             "It worked!"
                                                 -- J. Robert Oppenheimer


 The Hitchhacker's Guide to Milliways Development
    (To be read in the voice of Peter Jones)
-------------------------------------------------
This is the best and worst guide to Milliways testing and development ever
written. It assumes that you are already familiar with git and know where to
clone the mw repository from. Whether you use your own public clone and send a
pull request, or you have permission to push to the main git tree, or use git
format-patch and git send-email is largely academic as no single git workflow
has been chosen (nor, more importantly, rejected). It also assumes that you're
familiar with running and using mw and have a general sense of how it works, or
at least a willingness to learn how it works for yourself.

This guide should be kept updated to reflect changes in workflows and the build
system, which is currently a strange tangle of context-sensitive GNU Make rules
grown organically over time to support some unconventional requirements. If you
find any problems with, or ways to improve this document, report a bug or
send/push a patch.

 The Condensed Version
-----------------------
 1. git clone .../mw.git
 2. cd mw/src
 3. make test
 4. workworkwork...
 5. make test
 6. ./mwserv -f &
 7. ./mw
 8. testtesttest... !qq
 9. if fail, goto 4
10. git add changed files...
11. git commit

 The Version Nobody Reads
--------------------------
The most difficult part of contributing to any open source project can be
finding something to work on. Therefore, the most useful thing to do when
starting out with a project (assuming you want to work on the code rather than
the docs or other supporting material) is to increase your chances of
triggering bugs. One way to do this with mw is to run your own test version, as
your user, from the source directory, so that you can throw all sorts of crazy
combinations of inputs at it without stepping on anyone's toes.  It also means
you can quickly test any changes in a tight develop-test cycle.

To get started, first clone the git tree and change into the src subdirectory:

 $ git clone .../mw.git
 $ cd mw/src

Then run the 'test' make rule, which first creates a directory (named mwtest by
default) into which an appropriate directory structure and mw's static files
(docs, banner, etc.) are installed. It then builds mw and mwserv, setting the
appropriate hardcoded paths to the new directory by passing them as -D options
to the compiler. [NB it would be useful and more flexible to add configuration
options and/or command line options to provide the paths in future, to simplify
or obsolete the separate 'test' rule.]

 $ make test

In order to build mw you will need the build dependencies installed on your
system. The best way to find out what these currently are is to look at the
build requirements listed in the RPM spec file 'mw.spec' at the top of the
source tree, or the debian-template/control file. You will also need basic
toolchain packages like gcc, g++ and GNU Make installed.

Now that you've completed your first mw build, you can run the 'mwserv' server
and the 'mw' client programs.

 $ ./mwserv

A note on mwserv daemonization: by default, mwserv quietly 'daemonizes' itself
by forking off and ending the parent process so that the child is not attached
to your terminal. This means that you will have to kill it with 'pkill mwserv'
or similar in order to shut it down.  Alternatively, and better for testing,
mwserv has a foreground mode which causes it to stay running in your terminal
and also turns on useful debugging messages. This can be enabled using the
--foreground or -f command line option, but if you intend to run mwserv often,
you might prefer to create a ~/.mwserv.conf file and set the "foreground"
option to true.  To generate an initial .mwserv.conf file containing mwserv's
default configuration, run ./mwserv -P which will print a JSON-formatted config
similar to:

 $ ./mwserv -P
 {
  "foreground": false,
  "port": 9999,
  "user": "mw"
 }

You can use this as the base for your ~/.mwserv.conf. You may also wish to
change the port number, particularly if you are testing on a shared development
machine, although this will mean you have to run the client (./mw) with the
-server option. The "user" option is really only useful when mw has been
deployed and is launched as the root user which will allow it to setuid() as
the specified user.  Once you've created this file, you can verify that mwserv
will pick up your changes by running './mwserv -P' again. With mwserv
configured to run in the foreground, run it with

 $ ./mwserv &

to send it into the background, or just run it in a separate terminal in the
foreground, so that you can then run

 $ ./mw

and begin your testing.

A note on mw's client/server status: mw started off as a peer-to-peer system
which meant that all mw processes had to be run with permission to read and
write the data files containing the BBS folder, messages, logs and user data.
As the transition to client/server is still ongoing this is largely still the
case, although much of the actual chat-related communication is now sent via a
custom protocol over TCP/IP sockets. Details of the protocol can be found in
the src/server/PROTOCOL file. After making changes and rebuilding with 'make
test' you will need to make sure your previously running mwserv process has
been killed before testing again.

To remove all of the built files in the src directory and below you can run
'make clean' in the src directory. This will leave your mwtest directory in
place. To remove that too, run 'make testclean'.

As a rule, the code that lives in the src directory is meant to be library code
which is common to the client and the server. It gets compiled into a static
libmw.a archive which makes it easier for the client and server to link against
it. The code in the src/client/ and src/server/ directories are specific to the
client and server respectively.

// TODO: What else would be useful to include here?
