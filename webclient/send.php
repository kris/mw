<?
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

require_once("config.php");

$_REQUEST = json_decode(file_get_contents('php://input'), true);

$data = unserialize($_REQUEST['mwsess']);
$sess = (int)$data['pid'];
$auth = trim($data['auth']);

$path = $poller_path.$sess;

header("Content-type: application/json");

if (!isset($_REQUEST['send'])) {
  echo "{\"status\":\"Bad command\"}\n";
  exit;
}

$sock = socket_create(AF_UNIX, SOCK_SEQPACKET, 0);
if (@socket_connect($sock, $path) === FALSE) {
  echo "{";
  echo "\"status\":\"Socket open error\"";
  $err = error_get_last();
  echo ",\"detail\":\"".$err['message']."\"";
  echo ",\"path\":\"".$path."\"";
  echo "}\n";
  exit;
}

$msg = "auth $auth";
if (@socket_send($sock, $msg, strlen($msg), 0)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

if (@socket_recv($sock, $response, 4096, 0)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

$res = json_decode($response, TRUE);
if ($res === NULL) {
  echo "{\"status\":\"Bad server response\"}\n";
  exit;
}

if ($res['status'] != "OK") {
  echo $response;
  exit;
}

$msg = trim($_REQUEST['send']);

if (@socket_send($sock, $msg, strlen($msg), MSG_EOF)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

if (@socket_recv($sock, $response, 8192, 0)===FALSE) {
  echo "{\"status\":\"Socket error\"}\n";
  exit;
}

if ($_REQUEST['send']=="who") {
  $who = json_decode($response, TRUE);
  $pcount=0;
  foreach ($who as $person) {
    $im_file = "/var/www/sucssite/htdocs/pictures/people/".strtolower($person['username']).".png";
    if (file_exists($im_file)) {
      $size = getimagesize($im_file);
      $factor = 64/$size[1];
      $who[$pcount]['hgwidth']=(int)($size[0]*$factor);
    }
    $pcount++;
  }
  $response = json_encode($who);
}

echo $response."\n";

socket_close($sock);
?>
