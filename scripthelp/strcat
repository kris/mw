[1;4mString Instructions[0m

[1mNAME[0m
     [1mSTRCHR[0m  - Locate single character in string
     [1mSTRNCHR[0m - Locate single non-rejected character in string
     [1mSTRSTR[0m  - Locate substring in string
     [1mSTRISTR[0m - Locate case insensitive substring in string
     [1mSTRMID[0m  - Extract substring from string
     [1mSTRLEN[0m  - Measure length of string
     [1mSTRCAT[0m  - Append two strings
     [1mTOUPPER[0m - Convert string to UPPERCASE
     [1mTOLOWER[0m - Convert string to lowercase

[1mSYNOPSIS[0m
     [1mSTRCHR[0m [4mvariable[0m [4mneedle[0m [4mhaystack[0m
     [1mSTRNCHR[0m [4mvariable[0m [4mreject[0m [4mstring[0m
     [1mSTRSTR[0m [4mvariable[0m [4mneedle[0m [4mhaystack[0m
     [1mSTRISTR[0m [4mvariable[0m [4mneedle[0m [4mhaystack[0m
     [1mSTRMID[0m [4mvariable[0m [4msection[0m [4mtext[0m
     [1mSTRLEN[0m [4mvariable[0m [4mtext[0m
     [1mSTRCAT[0m [4mvariable[0m [4mtext[0m
     [1mTOUPPER[0m [4mvariable[0m [4mtext[0m
     [1mTOLOWER[0m [4mvariable[0m [4mtext[0m

[1mDESCRIPTION[0m
     [1mSTRCHR[0m  - Search haystack for the first occurance of any character in
               needle, and place its index value into variable.

     [1mSTRNCHR[0m - Search string for the first occurance of any character NOT in
               reject, and place its index value into variable.

     [1mSTRSTR[0m  - Search haystack for the first occurance of the substring
               needle, and place the index of its start into variable.

     [1mSTRISTR[0m - Search haystack for the first occurance of the substring
               needle without case sensitivity, and place the index of
               its start into the given variable.

     [1mSTRMID[0m  - Place the substring of [4mtext[0m as defined by section into
               the variable.

               section is defined as 'start:end'  where start and end
               are the index values of the substring required, if their
               is ommited, the start/end of the whole string is assumed.
               If they are the same, then a single character is copied.

     [1mSTRLEN[0m  - This function sets [4mvariable[0m to the value of the length of the
               text

     [1mSTRCAT[0m  - This function appends [4mtext[0m to the contents of variable

     [1mTOUPPER[0m - This function converts [4mtext[0m to UPPERCASE and places
               the result in [4mvariable[0m.

     [1mTOLOWER[0m - This function converts [4mtext[0m to lowercase and places
               the result in [4mvariable[0m.

