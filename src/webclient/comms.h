#include <ipc.h>

/* comms.c */
void open_command_socket(void);
int mainloop(int millis);
char *json_escape(char *original);
void close_cmd(void);
void talk_rawbcast(const char *fmt, ...) __attribute__((format(printf,1,2)));
void create_user(struct user *me, const char *username, const char *password);
const char * fetch_who(int immediate);
int32_t who_find(const char *username);
