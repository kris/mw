#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <unistd.h>

__attribute__((unused)) static inline void cleanup_free(void *x)
{
	free(*((void **)x));
}
#define _autofree __attribute__((cleanup(cleanup_free)))

#define SAFE_FREE(a)	do { if (a) { free(a); (a) = NULL; } } while(0);

#endif /* UTIL_H */
