#ifndef MAIN_H
#define MAIN_H

void close_down(int exitmode, char *sourceuser, char *reason);
void display_message(const char *text, int beeps, int newline);
void format_message(const char *format, ...) __attribute__((format(printf,1,0)));
void printfile(const char *filename);
void broadcast_onoffcode(int code, int method, const char *sourceuser, const char *reason);
void reset_timeout(int secs);
int idle(int fd, int millis);
void set_rights(void);
char *dupstr(const char *text, const char *prepend);

char *part_user(const char *text, int status);
char *part_comm_search(const char *text, int status);
char *part_comm_folder(const char *text, int status);
char *part_comm_user(const char *text, int status);
char *part_comm_mesg(const char *text, int status);
char *list_commands(const char *text, int state);
char *list_chat_commands(const char *text, int state);
char *find_folder(const char *text, int state);

void devel_msg(const char *func, const char *fmt, ...) __attribute__((format(printf,2,3)));

int disable_rl(int savetext);

#endif /* MAIN_H */
