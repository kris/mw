#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "expand.h"

/* New arg expansion code */

#define EA_BRACE	1
#define EA_BRACKET	2
#define EA_PAREN	4
#define EA_COLON	8

/* Debug macros / vars */

#define SHOWSTATE() /* printf("%-20s%20s\n", exp_buf.start, in) */

static struct expand_buf exp_buf = {NULL,NULL,0};
static var_op_t exp_var;
static char *var_key = NULL;

static void expand_init(void);
static void ensure_capacity(int cap);
static void append_chars(const char *str, int len);
static void append_string(const char *str);
static const char *expand_arg(const char *in, int flags);
static const char *expand_variable(const char *in);
static const char *expand_identifier(const char *in);
static const char *expand_name_string(const char *in);
static const char *expand_index(const char *in, var_list_t *vars);
static void do_lookup(const char *start, const char *end);
static const char *expand_infix(const char *in);
static const char *eval_expr(const char *in);
static void argv_squish(int offset);

static void expand_init(void)
{
    exp_buf.pos = exp_buf.start;
    ensure_capacity(0);
    *exp_buf.start = '\0';
}

void expand_close(void)
{
	if (var_key) free(var_key);
	if (exp_buf.start) free(exp_buf.start);
}

const char *eval_str(char *arg)
{
    expand_init();
    if (expand_arg(arg, EA_BRACE|EA_BRACKET|EA_PAREN))
	return exp_buf.start;
    else
	return NULL;
}

int eval_var(const char *name, var_op_t *op)
{
    expand_init();
    if (expand_variable(name))
    {
	*op = exp_var;
	var_key_dup(op);
	return 1;
    } else
        return 0;
}

static void append_chars(const char *str, int len)
{
    ensure_capacity(len);
    memcpy(exp_buf.pos, str, len * sizeof(char));
    *(exp_buf.pos += len) = '\0';
}

static void append_string(const char *str)
{
    static int len;

    len = strlen(str);
    ensure_capacity(len);
    strcpy(exp_buf.pos, str);
    exp_buf.pos += len;
}

static void ensure_capacity(int cap)
{
    static int offset;

    offset = exp_buf.start? exp_buf.pos - exp_buf.start: 0;
    cap += offset + 1;
    if (!exp_buf.start || exp_buf.capacity < cap)
    {
	exp_buf.capacity = ((cap-1) | 0xfff) + 1; /* Round up to 4Kb */
	exp_buf.start = realloc(exp_buf.start, exp_buf.capacity);
	exp_buf.pos = exp_buf.start + offset;
    }
}

static const char *expand_arg(const char *in, int flags)
{
    static int length;

    SHOWSTATE();
    /*
    printf("Entering expand_arg()\n");
    ensure_capacity(2+strlen(in));
    */
    while (*in)
    {
        switch(*in)
	{
	/* Brackets within bracketed expressions eg "$(2*(1+3))" must be
	 * balanced, for obvious reasons.
	 */
	case '{':
	    if (!(flags & EA_BRACE))
            {
		append_chars(in++, 1);
		in = expand_arg(in, flags | ~EA_BRACE);
		if (in && *in) append_chars(in++, 1);
		break;
	case '[':
		if (!(flags & EA_BRACKET))
		{
		    append_chars(in++, 1);
		    in = expand_arg(in, flags | ~EA_BRACKET);
		    if (in && *in) append_chars(in++, 1);
		    break;
	case '(':
		    if (!(flags & EA_PAREN))
		    {
			append_chars(in++, 1);
			in = expand_arg(in, flags | ~EA_PAREN);
			if (in && *in) append_chars(in++, 1);
			break;
		    }
		}
	    }
	    append_chars(in++, 1);
	    break;
	/* Return on the last closing bracket */
	case '}':
	    if (!(flags & EA_BRACE))
	    {
		/*printf("Encountered brace; leaving expand_arg()\n");*/
	        return in;
	case ']':
		if (!(flags & EA_BRACKET))
		{
		    /*printf("Encountered bracket; leaving expand_arg()\n");*/
		    return in;
	case ')':
		    if (!(flags & EA_PAREN))
		    {
			/*printf("Encountered parenthesis; leaving expand_arg()\n");*/
			return in;
		    }
		}
	    }
	    append_chars(in++, 1);
	    break;
	default:
	    length = strcspn(in, "$[]{}()");
	    append_chars(in, length);
	    in += length;
	    break;
	case '$':
	    switch (*++in)
	    {
	    case '$':
	        append_chars(in++, 1);
		break;
	    case '*':
	        argv_squish(0);
		SHOWSTATE();
		++in;
		break;
	    case '%':
	        argv_squish(1);
		SHOWSTATE();
		++in;
		break;
	    case '^':
	        append_chars("$^", 2); /* Preserve for argv_shift() */
		++in;
		break;
	    case '[':
		/*printf("Encountered '['; Expanding arithmetic expression\n");*/
		in = expand_infix(++in); /* Quoted variable name */
		if (!in) return NULL; /* Parse error */
		if (*in) /* Allow omitting ']' at the end of an expression */
		{
		    if (*in == ']') ++in; /* Skip over ']' */
		    else
		    {
			SHOWSTATE();
			/*printf("ERROR: ']' expected\n");*/
			return NULL; /* Parse error */
		    }
		}
		break;
	    case '|': /* Shortcut for 'strlen' or array size */
		ensure_capacity(30); /* Make room for the number */
		in = expand_variable(++in);
		if (!in) return(NULL);
		if (VAR_TYPE_STR(&exp_var) || VAR_TYPE_INT(&exp_var))
		{
		    /*printf("Inserting string length\n");*/
		    exp_buf.pos += snprintf(exp_buf.pos, 29, "%zu", strlen(var_str_val(&exp_var)));
		} else if (VAR_TYPE_ARRAY(&exp_var))
		{
		    static int count;

		    var_list_t *array = VAR_ARRAY_VAL(&exp_var);
		    count = 0;
		    VAR_LIST_ITERATE(&exp_var, array);
		    while (VAR_FOUND(&exp_var))
		    {
			++count;
			VAR_LIST_NEXT(&exp_var);
		    }
		    /*printf("Inserting array size\n");*/
		    exp_buf.pos += snprintf(exp_buf.pos, 29, "%d", count);
		} else
		    append_string("");
		SHOWSTATE();
		break;
	    default:
		in = expand_variable(in);
		if (!in) return NULL;
		/* We should have a string now */
		if (VAR_TYPE_STR(&exp_var) || VAR_TYPE_INT(&exp_var))
		    append_string(var_str_val(&exp_var));
		else
		    append_string("");
		    /* TODO: add sane expansions for arrays etc */
		/*printf("Inserting variable value\n");*/
		SHOWSTATE();
		break;
	    }
	    break;
	}
    }
    return in;
}

static const char *expand_variable(const char *in)
{
    SHOWSTATE();
    /*printf("Entering expand_variable()\n");*/
    switch(*in)
    {
    default:
	/* Start of an ordinary variable name */
        in = expand_identifier(in);
	break;
    case '#':
        /* Number of args passed to function */
        do_lookup(in, in+1);
	++in;
	break;
    case '{':
	/*printf("Encountered '{'; Expanding name\n");*/
        in = expand_name_string(++in); /* Quoted variable name */
	if (in && *in) /* Allow omitting '}' at the end of an arg */
	{
	    if (*in == '}') ++in; /* Skip over '}' */
	    else
	    {
		SHOWSTATE();
		/*printf("ERROR: '}' expected\n");*/
		return NULL; /* Parse error */
	    }
	}
	break;
    }
    if (!in) return in;
    /* Now check for array reference(s) */
    while (1 == 2 && *in == '[') /* disabled for now */
    {
	/*printf("Encountered '['; ");*/
        /* Find the array to search */
	if (VAR_TYPE_ARRAY(&exp_var))
	{
	    /*printf("Parsing array index\n");*/
	    /* Search for and expand the array element */
	    in = expand_index(++in, VAR_ARRAY_VAL(&exp_var));
	    SHOWSTATE();
	} else
	{
	    /* Leave strings alone for now.  i.e. if $s == "fish" then */
	    /* $s[wibble] == "fish". */
	    /* Maybe we could add string slicing i.e. $s[2:3] == "is"? */
	    char *garbage_start = exp_buf.pos;
	    /*printf("Not an array - will ignore index\n");*/
	    in = expand_arg(++in, EA_BRACE|EA_PAREN);
	    *(exp_buf.pos = garbage_start) = '\0';
	}

	if (*in) /* Allow omitting ']' at the end of an arg */
	{
	    if (*in == ']') ++in; /* Skip over '}' */
	    else
	    {
		SHOWSTATE();
		/*printf("ERROR: ']' expected\n");*/
		return NULL; /* Parse error */
	    }
	}
    }
    SHOWSTATE();
    /*printf("Leaving expand_variable().\n");*/
    return in;
}

/* Expand a simple identifier (alphanumerics and '_' only)
 * IN:   (none)
 * OUT:  name_start: Pointer to the expanded name within exp_buf
 *       var_key:    Copy of the expanded name
 * ARGS: in:         The starting parse position in the input string
 * RETURN:           The end parse position
 */
static const char *expand_identifier(const char *in)
{
    static const char *name_start;
    static char c;

    SHOWSTATE();
    /*printf("Entering expand_identifier()\n");*/
    name_start = in;
    /* Optimisation - isalnum() is *slow* */
    while (c = *in | 0x20,
           (c >= 'a' && c <= 'z') ||
	   (c >= '0' && c <= '9') ||
	   *in == '_' ||
	   *in == '/')
	++in;
    /*while (isalnum(*in) || *in == '_') ++in;*/

    SHOWSTATE();
    /* We have an identifier; look it up in the variable list */
    /*printf("Leaving expand_identifier()\n");*/
    if (in <= name_start)
    {
        int len = strlen(in);
	if (len>10) len=10;
	snprintf(parse_error, 79, "Expected identifer, found \"%*s\"", len, in);
        return NULL;
    } else
    {
	/*printf("Looking up key: %s.\n", name_start);*/
	do_lookup(name_start, in);
	/*printf("Looked up key: %s.\n", name_start);*/
	return in;
    }
}

static const char *expand_name_string(const char *in)
{
    char *name_start;
    var_op_t saved_op;
    char *saved_key;

    SHOWSTATE();
    /*printf("Entering expand_name_string()\n");*/
    /* Recursively expand the quoted name */
    saved_op = exp_var;
    saved_key = var_key;
    name_start = exp_buf.pos;
    var_key = NULL;
    in = expand_arg(in, EA_BRACKET|EA_PAREN);
    if (var_key) free(var_key);
    var_key = saved_key;
    exp_var = saved_op;
    if (!in) return NULL; /* Propagate parse errors */
    /* We have an identifier; look it up in the variable list */
    do_lookup(name_start, exp_buf.pos);
    SHOWSTATE();
    /*printf("Lookup complete; popping name\n");*/
    *(exp_buf.pos = name_start) = 0; /* 'pop' the variable name */
    SHOWSTATE();
    /*printf("Leaving expand_name_string()\n");*/
    return in;
}

/* Expand a key and search an array for it
 * IN:   (none)
 * OUT:  var_key:    Copy of the key
 *       exp_var:    Pointer to an array element if found
 *       name_start: Pointer to the key in the output string
 * ARGS: in:         The starting parse position in the input string
 *       array:      The array to be searched
 * RETURN:           The end parse position
 */
static const char *expand_index(const char *in, var_list_t *array)
{
    char *name_start;
    var_op_t saved_op;
    char *saved_key;

    SHOWSTATE();
    /*printf("Entering expand_index()\n");*/
    /* Recursively expand the quoted name */
    saved_op = exp_var;
    saved_key = var_key;
    name_start = exp_buf.pos;
    var_key = NULL;
    in = expand_arg(in, EA_BRACE|EA_PAREN);
    if (var_key) free(var_key);
    var_key = saved_key;
    exp_var = saved_op;
    if (!in) return NULL; /* Propagate parse errors */

    /* We have a key; look it up in the array */
    if (var_key) free(var_key);
    var_key = calloc(exp_buf.pos + 1 - name_start, sizeof(char));
    /*printf("expand_index: searching for key '%*s'\n", exp_buf.pos - name_start, name_start);*/
    strncpy(var_key, name_start, exp_buf.pos - name_start);
    VAR_OP_INIT(&exp_var, array);
    VAR_SEARCH(&exp_var, var_key);

    SHOWSTATE();
    /*printf("Lookup complete; popping name\n");*/
    *(exp_buf.pos = name_start) = 0; /* 'pop' the array index */
    SHOWSTATE();
    /*printf("Leaving expand_index()\n");*/
    return in;
}

/* Look up the variable name pointed to by `name_start'
 * OUT: var_key:    Copy of the variable name
 *      exp_var:    Pointer to a local/global variable if found
 */
static void do_lookup(const char *start, const char *end)
{
    if (var_key) free(var_key);
    var_key = calloc(end + 1 - start, sizeof(char));
    strncpy(var_key, start, end - start);
    /*printf("do_lookup: searching for variable '%s'\n", var_key);*/
    VAR_OP_INIT(&exp_var, local_vars);
    VAR_SEARCH(&exp_var, var_key);
    if (!VAR_FOUND(&exp_var))
    {
        VAR_OP_INIT(&exp_var, &var_list);
	VAR_SEARCH(&exp_var, var_key);
    }
}

static void argv_squish(int offset)
{
    int i;
    int argc;
    static char number[10];
    static var_op_t op;

    argc = arg_count(local_vars);

    for (i=1;i<(argc-offset);i++)
    {
	snprintf(number, 9, "%d", i+offset);
	VAR_OP_INIT(&op, local_vars);
	VAR_SEARCH(&op, number);
	if (VAR_FOUND(&op))
	{
	    if (i>1) append_chars(" ", 1);
	    append_string(var_str_val(&op));
	}
    }
}

typedef enum
{
    op_nop = 0,
    op_notb = 0x10, op_uminus = 0x11,  op_uplus = 0x12,
    op_times = 0x20, op_div = 0x21, op_mod = 0x22,
    op_plus = 0x30, op_minus = 0x31,
    op_shl = 0x40, op_shr = 0x41, op_ushr = 0x42,
    op_andb = 0x70, op_xorb = 0x71, op_orb = 0x72,
    op_paren = 0x200, op_end = 0x210
} operator_t;

static long numstack[1024];
static operator_t opstack[1024];
static int stack_limit = 0;
static int numcount = 0, opcount = 0;
static int complete = 0;
char parse_error[80] = {0};

static int push_op(operator_t op)
{
    if ((op < 0x20 && complete) ||
        (op >= 0x20 && !complete))
    {
	snprintf(parse_error, 79, "Invalid operator #%d: complete=%d", op, complete);
	return 0; /* Parse error */
    }
    /*printf("Pushing operator #%d.\n", op);*/
    /*printf("Op stack size: %d; top: %d; new: %d.\n", opcount, (opstack[opcount-1] & ~0xf), (op & ~0xf));*/
    while (opcount > stack_limit &&
           (opstack[opcount-1] & ~0xf) <= (op & ~0xf))
    {
	--opcount;
	if (opstack[opcount] >= op_paren &&
	    opstack[opcount] >= op)
	{
	    break;
	} else switch(opstack[opcount])
	{
	case op_uplus:
	    break; /* No calculation necessary */
	case op_notb:
	    numstack[numcount-1] = ~numstack[numcount-1];
	    break;
	case op_uminus:
	    numstack[numcount-1] = -numstack[numcount-1];
	    break;
	case op_times:
	    numstack[numcount-2] *= numstack[numcount-1];
	    --numcount;
	    break;
	case op_div:
	    numstack[numcount-2] /= numstack[numcount-1];
	    --numcount;
	    break;
	case op_mod:
	    numstack[numcount-2] %= numstack[numcount-1];
	    --numcount;
	    break;
	case op_plus:
	    numstack[numcount-2] += numstack[numcount-1];
	    --numcount;
	    break;
	case op_minus:
	    numstack[numcount-2] -= numstack[numcount-1];
	    --numcount;
	    break;
	case op_shl:
	    numstack[numcount-2] <<= numstack[numcount-1];
	    --numcount;
	    break;
	case op_shr:
	    numstack[numcount-2] >>= numstack[numcount-1];
	    --numcount;
	    break;
	case op_ushr:
	    numstack[numcount-2] = (unsigned long)numstack[numcount-2]
	    			   >> numstack[numcount-1];
	    --numcount;
	    break;
	case op_andb:
	    numstack[numcount-2] &= numstack[numcount-1];
	    --numcount;
	    break;
	case op_xorb:
	    numstack[numcount-2] ^= numstack[numcount-1];
	    --numcount;
	    break;
	case op_orb:
	    numstack[numcount-2] |= numstack[numcount-1];
	    --numcount;
	    break;
	default:
	    /* All remaining operators are no-ops */
	    break;
	}
	/*printf("Reducing to value: %d\n", numstack[numcount-1]);*/
	/*printf("Op stack size: %d; top: %d.\n", opcount, (opstack[opcount-1] & ~0xf));*/
    }
    /*printf("Leaving push_op(); complete=%d.\n", complete);*/

    complete = (op >= op_paren); /* There are no postfix operators */

    if (!complete) /* Don't keep '(' after evaluating contents */
	opstack[opcount++] = op;

    return 1; /* Errors caught earlier */
}

static int push_num(int num)
{
    if (complete)
    {
	snprintf(parse_error, 79, "Operator expected");
	/*printf("Operator expected.\n");*/
	return 0; /* Parse error */
    } else
    {
	/*printf("Pushing value: %d.\n", num);*/
	numstack[numcount++] = num;
	complete = 1;
	return 1;
    }
}

static int push_paren(void)
{
    if (complete)
    {
	/*printf("Error pushing '('.\n");*/
	return 0; /* Parse error */
    } else
    {
	opstack[opcount++] = op_paren;
	return 1;
    }
}

static const char *expand_infix(const char *in)
{
    char *expr_start;
    var_op_t saved_op;
    char *saved_key;
    char *expr_text;
    int saved_limit;

    SHOWSTATE();
    /*printf("Entering expand_infix()\n");*/

    /* Recursively expand the expression text */
    expr_start = exp_buf.pos;
    saved_op = exp_var;
    saved_key = var_key;
    var_key = NULL;
    saved_limit = stack_limit;
    stack_limit = opcount;
    in = expand_arg(in, EA_PAREN|EA_BRACE);
    complete = 0; /* Prepare for next expansion */
    stack_limit = saved_limit;
    if (var_key) free(var_key);
    var_key = saved_key;
    exp_var = saved_op;
    if (!in) return NULL; /* Propagate parse errors */

    /* We have an expression; Copy and evaluate it */
    SHOWSTATE();
    expr_text = calloc(exp_buf.pos + 1 - expr_start, sizeof(char));
    strncpy(expr_text, expr_start, exp_buf.pos - expr_start);

    /*printf("Expression copied; removing from buffer\n");*/
    *(exp_buf.pos = expr_start) = 0; /* 'pop' the original expression */
    if (!eval_expr(expr_text))
    { /* Error in expression */
	free(expr_text);
	return NULL;
    }
    SHOWSTATE();
    /*printf("Leaving expand_infix()\n");*/

    free(expr_text);
    return in;
}

static const char *eval_expr(const char *in)
{
    static int num;

    SHOWSTATE();
    /*printf("Entering eval_expr()\n");*/
    while (*in)
    {
        if (isdigit(*in))
	{
	    /* Parse & push a number */
	    if (!push_num(strtol(in, (char **)&in, 0)))
	        return NULL; /* Parse error */
	} else if (isalpha(*in) || *in == '{')
	{
	    /* Parse & push a variable. */
	    if ((in = expand_variable(in)) != NULL)
	    {
		/*printf("Checking variable '%s'.\n", VAR_STR_KEY(&exp_var));*/
	        if (var_isanum(&exp_var, &num, 0))
		{
		    if (!push_num(num))
		    {
		        snprintf(parse_error, 79, "Expected operator, found \"%8s\"", in);
		    }
		} else
		{
		    if (VAR_FOUND(&exp_var))
			    snprintf(parse_error, 79, "Value '%s' of variable '%s' is not a number", var_str_val(&exp_var), VAR_STR_KEY(&exp_var));
		    else
			    snprintf(parse_error, 79, "Variable '%s' not found", var_key);
		    return NULL; /* Not a number */
		}
	    } else
	    {
		snprintf(parse_error, 79, "expand_variable() failed");
		return NULL; /* Parse error */
	    }
	} else if (isspace(*in))
	{
	    ++in;
	} else switch (*in++)
	{
	case '(':
	    if (push_paren()) break; else return NULL;
	case ')':
	    if (push_op(op_paren))
	    {
	        /*printf("Remaining input: \"%s\"\n", in);*/
		break;
	    } else
	    {
	        /*printf("Push of ')' failed!\n");*/
	       return NULL;
	    }
	case '~': /* ~ is always unary */
	    if (push_op(op_notb)) break; else return NULL;
	case '+': /* + and - have unary and binary variants */
	    if (complete)
	    {
	        if (!push_op(op_plus))
		    return NULL;
	    } else
	        if (!push_op(op_uplus))
		    return NULL;
	    break;
	case '-':
	    if (complete)
	    {
	        if (!push_op(op_minus))
		    return NULL;
	    } else
	        if (!push_op(op_uminus))
		    return NULL;
	    break;
	case '*': /* Others are always binary */
	    if (push_op(op_times)) break; else return NULL;
	case '/':
	    if (push_op(op_div)) break; else return NULL;
	case '%':
	    if (push_op(op_mod)) break; else return NULL;
	case '^':
	    if (push_op(op_xorb)) break; else return NULL;
	case '&':
	    if (push_op(op_andb)) break; else return NULL;
	case '|':
	    if (push_op(op_orb)) break; else return NULL;
	default:
	    snprintf(parse_error, 79, "Unexpected char in expression: \"%-12s...\"", --in);
	    return NULL; /* Parse error */
	}
    }

    /*printf("Op stack size: %d; top: %d.\n", opcount, (opstack[opcount-1] & ~0xf));*/
    if (push_op(op_end))
    {
	ensure_capacity(30);
        exp_buf.pos += snprintf(exp_buf.pos, 29, "%ld", numstack[--numcount]);
	return in;
    } else
    {
	/* Stop errors affecting later expansions */
	opcount = numcount = stack_limit = 0;
	snprintf(parse_error, 79, "Unexpected end of expression");
        return NULL;
    }
}
