#ifndef ADD_H
#define ADD_H

#include "user.h"

int add_msg(int folnum, struct user *user, int replyto);

#endif /* ADD_H */
