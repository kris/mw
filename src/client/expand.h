#ifndef EXPAND_H
#define EXPAND_H

#include "script.h"

struct expand_buf
{
    char *start, *pos;
    int capacity;
};

/*typedef int var_func_t(var_op_t *op, void *arg);*/
extern char parse_error[];

const char *eval_str(char *arg);
/*int call_with_var_op(char *varname, var_func_t *func, void *arg);*/
int eval_var(const char *name, var_op_t *op);
void expand_close(void);

#endif /* EXPAND_H */
