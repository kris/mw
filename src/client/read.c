/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include "strings.h"
#include "str_util.h"
#include "perms.h"
#include "read.h"
#include "talker.h"
#include "echo.h"
#include "util.h"
#include "bb.h"
#include "userio.h"

extern int remote;
extern FILE *output;

int get_data(int afile, struct Header *tmp)
{
	int no;
	no=read(afile,tmp,sizeof(*tmp));
	if (!no) return(false); else
	if (no<sizeof(*tmp))
	{
		printf("Failed to read entire record. Folder Damaged.");
		perror("read");
		return(0);
	}
	return(true);
}

void display_article(struct Header *tmp, int datafile)
{
	int i,length;
	char *buff,*ptr;
	char title[80];
	time_t when;

	buff=(char *)malloc(tmp->size);

	if (!remote)
	{
		when = tmp->date;
		sprintf(title,"Message %d from %s on %s",
		       tmp->Ref,tmp->from,ctime(&when));
		if ((ptr=(char *)strchr(title,'\n'))!=NULL) *ptr='.';
		fprintf(output,"\n%s\n",title);
		length=strlen(title);
		for (i=0;i<length;i++) fputc('-',output);
		fprintf(output,"\n");
	}else
	{
		printf("Message: %d\nFrom: %s\nDate: %ld\n",tmp->Ref,tmp->from,(long)tmp->date);
		show_mesg_stats(tmp->status,title,false);
		printf("Flags: %s\n",title);
	}
	fprintf(output,"To: %s\nSubject: %s\n",tmp->to,tmp->subject);

	if (tmp->replyto>0)
		fprintf(output,"In reply to message %d.\n",tmp->replyto);
	if (tmp->status&(1<<2))
		fprintf(output,"Message has a reply.\n");
	fprintf(output,"\n");
	lseek(datafile, tmp->datafield, SEEK_SET);
	if (read(datafile,buff,tmp->size)<tmp->size)
	{
		printf("Failed to read entire data entry");
		perror("read data");
		free(buff);
		return;
	}
	if (!remote)
	{
		if (output!=stdout) fprintf(output,"%s",buff);
		else
		more(buff,tmp->size);
		printf("\n");
	}else
	{
		char *p;
		p=buff;
		while (*p)
		{
			putchar(*p);
			if (*p=='\n' && p[1]=='.') putchar('.');
			p++;
		}
		printf("\n.\n");
	}
	free(buff);
}

int read_msg(int folnum, int msgnum, struct user *user)
{
	int file,headfile,textfile,i;
	struct folder data;
	struct Header head;
	int posn;
	long tmppos;

	if (nofolders())
		{printf("There are no folders to read.\n");return(false);}

	file=openfolderfile(O_RDONLY);
	memset(&data, 0, sizeof(struct folder));
	for (i=0;i<=folnum;i++) get_folder_entry(file,&data);

	close(file);

	if (!(data.status&1))
		{printf("That folder does not exist.\n");return(false);}
	if (msgnum>data.last || msgnum<data.first)
		{printf("There is no message %d.\n",msgnum);return(false);}
	if (data.last<data.first || data.last==0)
		{printf("Folder %s is empty.\n",data.name);return(false);}
	if ( !u_god(user) && !allowed_r(&data,user))
		{printf("You are not permitted to read this folder.\n");return(false);}
	if ((headfile = open_folder_index(&data, FOL_LIVE, O_RDONLY, 0)) < 0)
		return false;
	if ((textfile = open_folder_text(&data, FOL_LIVE, O_RDONLY, 0)) < 0) {
		close(headfile);
		return false;
	}
	tmppos=lseek(headfile, 0, SEEK_END);
	posn=(msgnum - data.last - 1)*sizeof(struct Header);
	if (posn < -tmppos)
		lseek(headfile, 0, SEEK_SET);
	else
		lseek(headfile, posn, SEEK_CUR);
	while (get_data(headfile,&head) && head.Ref<msgnum);
	if (head.Ref==msgnum)
	{
		if (u_god(user))
			display_article(&head,textfile);
		else
		if (head.status&(1<<1))
		{
			printf("Message has been marked for deletion.\n");
		}else
		if (is_private(&data,user) &&
		  (!stringcmp(head.to,user->record.name,-1)&&!stringcmp(head.from,user->record.name,-1)))
		{
			printf("This message is not addressed to you !\n");
		}else
			display_article(&head,textfile);
	}
	else
	{
		printf("Message %d does not exist.\n",msgnum);
	}
	close(headfile);
	close(textfile);
	return(true);
}

void ls(int folnum, struct user *user, int many)
{
	int afile;
	struct folder fold;
	struct Header head;
	int linecount=0;
	int listpoint;
	int screen_height = screen_h();

	if (!get_folder_number(&fold,folnum))
		return;

	if ((afile = open_folder_index(&fold, FOL_LIVE, O_RDONLY, 0)) < 0)
		return;
	if (many>0)
	{
		listpoint=fold.first+many;
		if (listpoint>fold.last) listpoint=fold.last;
	}else
	if (many<0)
	{
		listpoint=fold.last+many;
		if (listpoint<fold.first) listpoint=fold.first;
	}else
		listpoint=fold.first;
	printf("Listing messages in folder %s\n",fold.name);
	printf(" Msg. %*s -> %*s  %-*s\n",NAMESIZE,"From",NAMESIZE,"To",SUBJECTSIZE,"Subject");
	while (get_data(afile,&head))
	{
		if (((many>0 && head.Ref<listpoint) || many==0
		  ||(many<0 && head.Ref>listpoint) ) && (head.Ref <= fold.last))
		if (!(head.status&(1<<1))
		&& (!is_private(&fold, user)|| u_god(user) ||
		(is_private(&fold, user) && (stringcmp(head.from, user->record.name, -1)
		      || stringcmp(head.to, user->record.name, -1))))) /*marked for deletion*/
		{
			printf("%4d  %*s -> %*s  ", head.Ref,
			       NAMESIZE, head.from,
			       NAMESIZE, head.to);
			printf("%.*s\n", SUBJECTSIZE, head.subject);
			linecount++;
			if (linecount>=(screen_height-1))
			{
				char foo[6];
				printf("---more---\r");
				echo_off();
				get_str(foo,5);
				echo_on();
				if (foo[0]=='q' || foo[0]=='Q')
					break;
				printf("            \r");
				linecount=0;
			}

		}
	}
	close(afile);
}

void more(char *buff, int size)
{
	char *ptr;
	int len=0,found=5;
	char tmp[2];
	int scr_h = screen_h();

	ptr=buff;
	while ((ptr+len)<(buff+size))
	{
		/* go through string till EOS, or until max allowed lines per 'more' */
		do {
			if (ptr[len]=='\n') found++;
			len++;
		} while ((ptr+len)<(buff+size) && found<=(scr_h-4));

		/* write string to screen */
		fwrite(ptr,len,1,stdout);

		/* if max allowed lines reached, then 'more' and wait for key */
		if (found>=(scr_h-4))
		{
			printf("--More--\r");
			echo_off();
			get_str(tmp,1);
			echo_on();
			if (tmp[0]=='q' || tmp[0]=='Q') return;
			printf("         \r");
			ptr+=len;
			len=found=0;
		}
	}
}

/* returns true if okay, false if failed for some reason */
int get_mesg_header(struct folder *data, int msgnum, struct Header *head)
{
	int		headfile;
	int		posn;
	long		tmppos;

	/* folder data doesnt exist */
	if (!data) return(false);
	if (!(data->status & 1)) return(false);

	/* no message of that number */
	if (msgnum > data->last || msgnum < data->first) return(false);

	/* empty folder */
	if (data->last < data->first || data->last == 0) return(false);

	if ((headfile = open_folder_index(data, FOL_LIVE, O_RDONLY, 0)) < 0)
		return false;

	/* no message - file too small */
	tmppos = lseek(headfile, 0, SEEK_END);

	/* locate to file data */
	posn=(msgnum - data->last - 1)*sizeof(struct Header);
	if (posn < -tmppos)
		lseek(headfile, 0, SEEK_SET);
	else
		lseek(headfile, posn, SEEK_CUR);

	/* read it */
	while (get_data(headfile, head) && head->Ref < msgnum);
	close(headfile);

	/* if we have the correct message, return true */
	return(head->Ref == msgnum);
}
