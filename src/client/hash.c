/* hash.c
 * hashtable functions
 */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "hash.h"

#define INITIALBUCKETS 256
#define INITIALFIELDS 64

static bucket_t *buckets;
union fieldrec *fields;
static int capacity;
static int maxfields; /* Size of the allocated array */
static int nextfield; /* Max field # used so far, +1 */
int code; /* Hashcode of last key searched for */

static void linkb(struct hashrec *rec, struct hashrec **bucket);
static void linkf(struct hashrec *rec, int field);
static int reccmp(const struct hashrec *rec, int field, const char *key);
static struct hashrec *hash_enter(const hash_op_t *op);

/*static struct hashrec *freerecs = 0;*/
static union fieldrec *freefields = NULL;

void hash_setup(void)
{
    maxfields = INITIALFIELDS;
    fields = calloc(maxfields , sizeof(*fields));
    nextfield = 0;
    capacity = INITIALBUCKETS;
    buckets = calloc(capacity, sizeof(*buckets));
}

void hash_shutdown(void)
{
    free(buckets);
    free(fields);
}

int hash_alloc(void)
{
    int newfield;

    if (freefields)
    {
        newfield = freefields - fields;
	freefields = freefields->gnext;
    } else {
        /* No freed fields; use a new one. */
	if (nextfield >= maxfields)
	{
	    maxfields <<= 1;
	    fields = realloc(fields, maxfields * sizeof(*fields));
	}
	newfield =  nextfield++;
    }
    memset(&fields[newfield], 0, sizeof(*fields));
    fields[newfield].data.inorder = 1;
    /* Link the new field into the root list */
    return newfield;
}

void hash_free(int field)
{
    static union fieldrec *frec;

    /* make sure its actually alloced first */
    if (field == -1) return;

    /* Make sure all variables are removed */
    hash_purge(field);

    frec = &fields[field];
    /* Relink into free list */
    frec->gnext = freefields;
    freefields = frec;
}

void hash_op_init (hash_op_t *op, int field)
{
    op->key = NULL;
    op->rec = NULL;
    op->field = field;
}

int hash_search(hash_op_t *op, const char *key)
{
    static struct hashrec **rec;
    static int cmp;
    static int field;

    field = op->field;
    hashcode(field, key);
    op->key = (char *)key;
    rec = &buckets[code & (capacity-1)].first;
    while (*rec && (cmp=reccmp(*rec, field, key)) != 0)
	rec = &(**rec).bnext;
    op->rec = rec;
    return (*rec != NULL);
}

void hash_op_forgetkey(hash_op_t *op)
{
    op->key = NULL;
}

static int hash_search_quick(hash_op_t *op, const char *key, struct hashrec *target)
{
    static struct hashrec **rec;
    static int field;

    field = op->field;
    hashcode(field, key);
    rec = &buckets[code & (capacity-1)].first;
    while (*rec && *rec != target)
	rec = &(**rec).bnext;
    op->rec = rec;
    return (*rec != NULL);
}

void hash_iterate(struct hashrec ***iter, int field)
{
    struct hashrec *old;

    *iter = &fields[field].data.firstrec;
    while (**iter && (***iter).type == RT_VOID)
    {
        old = **iter;
	**iter = (***iter).fnext;
	free(old);
    }
}

void hash_next(struct hashrec ***iter)
{
    struct hashrec *old;

    *iter = &(***iter).fnext;
    while (**iter && (***iter).type == RT_VOID)
    {
        old = **iter;
	**iter = (***iter).fnext;
	free(old);
    }
}

void hash_purge(int field)
{
    struct hashrec **rec;
    hash_op_t op;

    hash_iterate(&rec, field);
    while (*rec)
    {
        hash_op_init(&op, field);
        hash_search_quick(&op, (**rec).key, *rec); /* Should always succeed */
	hash_delete(&op); /* Free keys & string values */
	hash_next(&rec);
    }
    hash_iterate(&rec, field); /* Free the hashrec structures */
}

static struct hashrec *hash_enter(const hash_op_t *op)
{
    struct hashrec *newrec;

    if (*op->rec)
    {
	newrec = *op->rec;
	if (newrec->type == RT_STRING && newrec->data.pdata)
		free(newrec->data.pdata);
    } else
    {
	newrec = malloc(sizeof(*newrec));
	newrec->field = op->field;
	newrec->key = strdup(op->key);
	linkb(newrec, op->rec);
	linkf(newrec, op->field);
    }
    return newrec;
}

void hash_set_p(hash_op_t *op, rectype_t type, const char *value)
{
    struct hashrec *newrec;

    newrec = hash_enter(op);
    newrec->type = type;
    newrec->data.pdata=NULL;
    if(value != NULL)
    {
		newrec->data.pdata = strdup(value);
	}
}

void hash_set_i(hash_op_t *op, rectype_t type, int value)
{
    struct hashrec *newrec;

    newrec = hash_enter(op);
    newrec->type = type;
    newrec->data.idata = value;
}

void hash_delete(hash_op_t *op)
{
    if ((**op->rec).type == RT_VOID)
        return; /* Already deleted */
    free((**op->rec).key);
    if ((**op->rec).type == RT_STRING)
        free((**op->rec).data.pdata);
    (**op->rec).type = RT_VOID;
    *op->rec = (**op->rec).bnext;
}

static void linkb(struct hashrec *rec, struct hashrec **bucket)
{
    /*rec->bprev = bucket;*/
    rec->bnext = *bucket;
    *bucket = rec;
}

static void linkf(struct hashrec *rec, int field)
{
    struct hashrec **pos; /* Ordered insert position */

    hash_iterate(&pos, field);
    if (fields[field].data.inorder)
    {
	while (*pos)
	{
	    if ((**pos).type == RT_VOID)
	    {
	        fprintf(stderr, "Urk, hash_next missed a deleted record\n");
		break;
	    } else if (strcasecmp(rec->key, (**pos).key) > 0)
	    	break; /* Found the right position to insert at */
	    hash_next(&pos);
	}
    }
    rec->fnext = *pos;
    *pos = rec;
}

static int reccmp(const struct hashrec *rec, int field, const char *key)
{
    int cmp;

    cmp = rec->field - field;
    if (cmp != 0)
	return cmp;
    else
	return strcasecmp(rec->key, key);
}

void hashcode(int field, const char *key)
{
    static char c;

    code = 1;
    while ((c = *key++) != '\0')
        code = code*(c|0x20) + 1;
    code = (code*35) ^ (field*33);
}
