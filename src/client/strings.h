#ifndef STRINGS_H
#define STRINGS_H

#define CMD_TEXT	0	/* Interpreted depending on mode/prefix      */
#define CMD_BOARD	1	/* Always a board command, used by BOARDEXEC */
#define CMD_TALKER	2	/* Always a talker command, used by EXEC     */
#define CMD_TYPED	0	/* Was entered at the keyboard               */

struct stacked_str {
	char *text;
	int type;
	struct stacked_str *next;
};

void stack_cmd(char *string, int type);
void stack_str(char *string);
int is_stacked(void);
void pop_cmd(char *string, int len, int *type);
int pop_stack(char *string, int len);

#endif /* STRINGS_H */
