#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <locale.h>

#include "folders.h"
#include "iconv.h"
#include "strings.h"
#include "str_util.h"
#include "main.h"
#include "perms.h"
#include "files.h"
#include "new.h"
#include "read.h"
#include "edit.h"
#include "colour.h"
#include "who.h"
#include "mod.h"
#include "getpass.h"
#include "add.h"
#include "init.h"
#include "talker.h"
#include "talker_privs.h"
#include "special.h"
#include "log.h"
#include "intl.h"
#include "sort.h"
#include "bb.h"
#include "alias.h"
#include "userio.h"
#include "mesg.h"
#include "incoming.h"
#include <util.h>

extern int currentfolder,last_mesg;
extern FILE *output;
extern int busy; /* if true dont display messages  i.e. during new/write */
extern unsigned long rights;
extern struct user * const user;
extern CommandList table[];

static void help(const char *topic, int wiz)
{
	CommandList	*c;

	if (topic!=NULL && (strchr(topic,'.')!=NULL || strchr(topic,'/')!=NULL))
	{
		printf(_("Sorry, no help available on that subject.\n"));
		return;
	}

	/* not given any topic to look for, so display general help */
	if (topic==NULL)
	{
		const char *x = NULL;

		/* allowed high-level help */
		if (wiz)
		{
			/* search for best help file */
			x = WIZHELP"/general";
			if (!access(x, 00)) printfile(x);
		}
		/* no high-level help allowed, or available */
		if (x == NULL)
		{
			/* search for normal help */
			x = HELPDIR"/general";
			if (!access(x, 00)) printfile(x);
		}
		/* display message as to whether help was found */
		if (x == NULL)
			printf(_("No general help available for talker commands.\n"));
	}
	/* a help topic was given */
	else
	{
		_autofree char *x = NULL;

		/* find the command in the list */
		c = table;
		while (c->Command && strcasecmp(c->Command, topic)) c++;

		/* do a permissions check if the command exists */
		if (c->Command)
		{
			if ((rights & c->Rights) != c->Rights)
			{
				printf(_("Sorry, no help available on that subject.\n"));
				return;
			}
		}
		/* search for the high-level help file */
		if (wiz)
		{
			/* search for the best topic help */
			asprintf(&x, WIZHELP"/%s", topic);
			if (x != NULL && !access(x, 00)) printfile(x);
		}
		/* no high-level help allowed, or available */
		if (x == NULL)
		{
			/* search for normal topic help */
			asprintf(&x, HELPDIR"/%s", topic);
			if (x != NULL && !access(x, 00)) printfile(x);
		}
		/* display the file, or appropriate help message */
		if (x == NULL)
			printf(_("Sorry, no help available on that subject.\n"));
	}
}

void c_help(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc>1)
		help(argv[1], u_god(user));
	else
		help_list(table, 1, CMD_BOARD_STR);
}

void c_cd(CommandList *cm, int argc, const char **argv, char *args)
{
	int i;
	if ((i=foldernumber(argv[1]))==-1)
		printf(_("Unknown foldername.\n"));
    	else
    	{
		struct folder *fold = &user->folder;

		get_folder_number(fold,i);
		if(!f_active(fold->status) || (!allowed_r(fold,user) && !allowed_w(fold,user)))
		{
			printf(_("Unknown foldername.\n"));
			get_folder_number(fold,currentfolder);
		}
		else
		{
			printf(_("Changing to folder %s.\n"),fold->name);
			currentfolder=i;
			last_mesg=0;
		}
	}
}

void c_addfol(CommandList *cm, int argc, const char **argv, char *args)
{
	add_folder();
}

void c_listnew(CommandList *cm, int argc, const char **argv, char *args)
{
	list_new_items(user, true);
}

void c_listall(CommandList *cm, int argc, const char **argv, char *args)
{
	list_new_items(user, false);
}

void c_new(CommandList *cm, int argc, const char **argv, char *args)
{
	busy++;
	show_new(user);
	busy--;
	get_folder_number(&user->folder, currentfolder);
	update_user(user);
}

void c_last(CommandList *cm, int argc, const char **argv, char *args)
{
	if (currentfolder < 0) {
		printf(_("No current folder.\n"));
		return;
	}
	last_mesg = user->folder.last;
	printf(_("Moved to end of folder. (message %d)\n"),last_mesg);
	read_msg(currentfolder, last_mesg, user);
}

void c_first(CommandList *cm, int argc, const char **argv, char *args)
{
	last_mesg = user->folder.first;
	printf(_("Moved to start of folder. (message %d)\n"),last_mesg);
	read_msg(currentfolder, last_mesg, user);
}

void c_user(CommandList *cm, int argc, const char **argv, char *args)
{
	busy++;
	edit_user(argv[1],argv[2]);
	busy--;
}

void c_search(CommandList *cm, int argc, const char **argv, char *args)
{
	search(argv[1],argv[2]);
}

void c_folder(CommandList *cm, int argc, const char **argv, char *args)
{
	edit_folder(argv[1],argv[2]);
}

void c_msg(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!u_mesg(user))
		{
			user->record.status |= (1 << MWUSR_MESG);
			printf(_("Messages are now off.\n"));
		}else
			printf(_("Messages are already off.\n"));
	}else
	if (z==1)
	{
		if (u_mesg(user))
		{
			user->record.status &= ~(1 << MWUSR_MESG);
			printf(_("Messages are now on.\n"));
		}else
			printf(_("Messages already on.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_inform(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!u_inform(user))
		{
			user->record.status |= (1 << MWUSR_INFORM);
			printf(_("You will NOT be informed of logins/outs.\n"));
		}else
			printf(_("You are already not informed of logins.\n"));
	}else
	if (z==1)
	{
		if (u_inform(user))
		{
			user->record.status &= ~(1 << MWUSR_INFORM);
			printf(_("You now WILL be informed of logins/outs\n"));
		}else
			printf(_("You are already informed of logins/outs.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_colouroff(CommandList *cm, int argc, const char **argv, char *args)
{
	int z;

	if (argc<2)
	{
		if (s_colouroff(user))
			printf(_("Colours disabled.\n"));
		else
			printf(_("Colour enabled.\n"));
		if (get_colour()==NULL)
			printf(_("No colour scheme loaded.\n"));
		printf(_("Current colour scheme: %d: %s\n"), user->record.colour, get_colour());

		return;
	}

	if (!strcasecmp(argv[1], "list"))
	{
		DIR *d;
		struct dirent *dd;
		int count;

		if ((d=opendir(COLOURDIR))==NULL)
		{
			printf(_("Error opening colour scheme list - %s\n"),strerror(errno));
			return;
		}
		count=0;
		printf(_("Listing available colour schemes:-\n"));
		while ((dd=readdir(d))!=NULL)
		{
			char *c, *fn;
			FILE *ff;
			char path[1024];

			fn=strdup(dd->d_name);
			c=strrchr(fn,'.');
			if (c==NULL || strcmp(c,".col")) { free(fn); continue; }
			*c=0;

			snprintf(path, 1023, COLOURDIR"/%s", dd->d_name);
			if ((ff=fopen(path,"r"))==NULL)
			{
				printf("%-3s: Error: %s\n", fn, strerror(errno));
				free(fn);
				continue;
			}
			fgets(path, 1023, ff);
			fclose(ff);

			printf("%-3s: %s", fn, path);
			count++;

			free(fn);
		}

		if (count==0) printf(_("No colour schemes found.\n"));
		closedir(d);
		return;
	}

	if (strchr("0123456789",argv[1][0]))
	{
		int i;
		char path[1024];

		i=atoi(argv[1]);
		snprintf(path, 1023, COLOURDIR"/%d.col", i);
		if (access(path, R_OK))
		{
			printf(_("ERROR: Cannot open colour scheme '%d'\n"),i);
			return;
		}

		colour_load(path, 0);
		user->record.colour = (unsigned char)i;
		return;
	}

	z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!s_colouroff(user))
		{
			user->record.special |= (1 << 8);
			printf(_("Colour mode now disabled.\n"));
		}else
			printf(_("Colour mode already disabled.\n"));
	}else
	if (z==1)
	{
		if (s_colouroff(user))
		{
			user->record.special &= ~(1 << 8);
			printf(_("Colour mode now enabled.\n"));
		}else
			printf(_("Colour mode already enabled.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_beep(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!u_beep(user))
		{
			user->record.status |= (1 << MWUSR_BEEPS);
			printf(_("You will NOT hear any beeps.\n"));
		}else
			printf(_("You have already turned beeps off.\n"));
	}else
	if (z==1)
	{
		if (u_beep(user))
		{
			user->record.status &= ~(1 << MWUSR_BEEPS);
			printf(_("You now WILL get beeps.\n"));
		}else
			printf(_("You are already getting beeps.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_wizchat(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (!s_chatoff(user))
		{
			user->record.special |= (1 << 3);
			printf(_("You will no longer receive wizchat.\n"));
		}else
			printf(_("You have already turned wizchat off.\n"));
	}else
	if (z==1)
	{
		if (s_chatoff(user))
		{
			user->record.special &= ~(1 << 3);
			printf(_("You will now receive wizchat messages.\n"));
		}else
			printf(_("You already receive wizchat messages.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_autosub(CommandList *cm, int argc, const char **argv, char *args)
{
	int t,n;

	t=BoolOpt(argv[2]);
	if (t==-1)
	{
		printf("%s\n",cm->ArgError);
		return;
	}
	printf(_("Forcing Subscription to folder %s "),t==0?_("Off"):_("On"));

	if ((n=foldernumber(argv[1]))==-1)
		printf(_("Failed\nUnknown folder name %s\n"),argv[1]);
	else
	{
		auto_subscribe(n,t);
		printf(_(", Done.\n"));
	}
}

void c_su(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);

	if (z==1)
	{
		if (!u_god(user))
		{
			user->record.status |= (1 << MWUSR_SUPER);
			update_user(user);
			printf(_("Wiz! Bang! - You're a wizard again.\n"));
		}else
			printf(_("You are already a wizard.\n"));
	}else
	if (z==0)
	{
		if (u_god(user))
		{
			user->record.status &= ~(1 << MWUSR_SUPER);
			update_user(user);
			printf(_("!gnaB !ziW - You feel rather normal.\n"));
		}else
			printf(_("You are already rather normal.\n"));
	}else
		printf("%s\n",cm->ArgError);
	set_rights();
}

void c_mesg(CommandList *cm, int argc, const char **argv, char *args)
{
	update_user(user);
	busy++;
	mesg_edit(argv[1], &user->folder, atoi(argv[2]), user);
	busy--;
	mwlog("MESSAGE %s %s:%d",argv[1], user->folder.name, atoi(argv[2]));
}

void c_latest(CommandList *cm, int argc, const char **argv, char *args)
{
	latest(user);
}

void c_who(CommandList *cm, int argc, const char **argv, char *args)
{
	update_user(user);
	display_wholist(0);
}

void c_wiz(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text, MAXTEXTLENGTH-1, "(%s): ", user->record.name);
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	broadcast(2, "%s", text);
}

void c_emote(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	snprintf(text, MAXTEXTLENGTH-1, "%s ", user->record.name);
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	broadcast(2, "%s", text);
}

void c_wall(CommandList *cm, int argc, const char **argv, char *args)
{
	char text[MAXTEXTLENGTH];

	text[0]=0;
	strncat(text, args, MAXTEXTLENGTH - strlen(text) - 1);
	broadcast(0, "%s", text);
}

void c_doing(CommandList *cm, int argc, const char **argv, char *args)
{
	if (argc > 1) {
		int overflow = (strlen(args) + 1) - DOINGSIZE;
		if (overflow > 0) {
			char status[DOINGSIZE];
			snprintf(status, DOINGSIZE, "%s", args);
			printf(_("Sorry, message is %d character%s too long (would be truncated to: '%s'). Try again.\n"), overflow, overflow > 1 ? "s" : "", status);
			return;
		}
		snprintf(user->record.doing, DOINGSIZE, "%s", args);
		user->record.dowhen = time(0);
		update_user(user);
		broadcast(5, "%s %s", user->record.name, user->record.doing);
		catchdoing(user->record.doing);
	} else {
		user->record.doing[0] = 0;
		user->record.dowhen = time(0);
		update_user(user);
		printf(_("You are marked as doing nothing.\n"));
	}
}

void c_pwd(CommandList *cm, int argc, const char **argv, char *args)
{
	if (currentfolder < 0) {
		printf(_("No current folder.\n"));
		return;
	}
	printf(_("Current folder = %s\n"), user->folder.name);
	if (last_mesg==0)
		printf(_("You haven't read any messages in this folder yet.\n"));
	else
		printf(_("You last read message %d.\n"),last_mesg);
}

void c_ls(CommandList *cm, int argc, const char **argv, char *args)
{
	int many;
	if (allowed_r(&user->folder, user))
	{
		if (argc>1) many=atoi(argv[1]);
		else many=0;
			ls(currentfolder, user, many);
	} else
		printf(_("You are not permitted to read this folder.\n"));
}

void c_topten(CommandList *cm, int argc, const char **argv, char *args)
{
	struct listing *head=NULL;
	struct Header hdr;
	struct folder folder;
	int file;
	int count;
	char buff[10];
	int screen_height = screen_h();

	if (argc!=2)
	{
		printf("Usage: topten <foldername>\n");
		return;
	}

	strncpy(folder.name, argv[1], FOLNAMESIZE);
	folder.name[FOLNAMESIZE] = '\0';

	if ((file = open_folder_index(&folder, FOL_LIVE, O_RDONLY, 0)) < 0)
		return;

	while(read(file,&hdr,sizeof(hdr))>0)
	{
		struct listing *listing = head;
		while (listing != NULL && strcasecmp(hdr.from, listing->name))
			listing = listing->next;

		if (listing != NULL)
		{
			listing->count++;
		 	listing->size+=hdr.size;
		}
		else
		{
			struct listing *lnew = calloc(1, sizeof(*lnew));
			strcpy(lnew->name,hdr.from);
			lnew->count=1;
			lnew->size=hdr.size;
			lnew->next=head;
			head=lnew;
		}
	}
	head=Sort(head);

	printf("Top posters in folder %s\n", folder.name);
	printf("In order of Total size of text posted.\n");

	count=2;
	while (head != NULL)
	{
		struct listing *l;

		printf("Name: %*s   %3d Msgs.   %5d bytes (%2.1fK)\n", NAMESIZE, head->name,
		       head->count, head->size, head->size/1024.0);
		count++;
		if (count>=screen_height-2)
		{
			printf("---more---\r");
			get_str(buff, 5);
			printf("          \r");
			if (*buff=='q' || *buff=='Q') exit(0);
			count=0;
		}
		l = head;
		head = head->next;
		free(l);
	}
}

void c_tidyup(CommandList *cm, int argc, const char **argv, char *args)
{
	struct Header idx;
	char *textbuff;
	int textin,textout;
	int indexin,indexout;
	int limit;
	int counter=0;

	int ffile;
	int i;
	int first=0xffff;
	struct folder info;

	printf(_("WARNING: This command should not be run whilst people are using\n"));
	printf(_("         the bulletin board, please exercise caution.\n"));
	printf("\n");

	if (argc!=3)
	{
	 	printf("Usage: tidyup <foldername> <mesg no>\n");
	 	return;
	}

	/* locate the folder that you want to tidy. */
	if ((ffile=openfolderfile(O_RDWR))<0)
	{
		perror("folders");
		exit(-1);
	}

	do {
		i=read(ffile,&info,sizeof(info));
	}while (i==sizeof(info) && !stringcmp(info.name,argv[1],-1));

	if (i<sizeof(info))
	{
		printf("Folder %s not found.\n",argv[1]);
		return;
	}

	/* display relavant info */
	printf("Folder: %s,  Topic: %s\n",info.name,info.topic);
	printf("Messages %d to %d\n",info.first,info.last);

	limit=atoi(argv[2]);
	if (limit<info.first || limit>=info.last)
	{
	 	printf("You must leave at least one message, or go and delete the folder.\n");
	 	return;
	}

	/* open relevant folder index */
	if ((indexin = open_folder_index(&info, FOL_LIVE, O_RDONLY, 0)) < 0)
		return;
	Lock_File(indexin);

	/* open relevant folder text */
	if ((textin = open_folder_text(&info, FOL_LIVE, O_RDONLY, 0)) < 0)
		return;
	Lock_File(textin);

	indexout = open_folder_index(&info, FOL_NEW, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR);
	if (indexout < 0)
		exit(-1);
	Lock_File(indexout);

	textout = open_folder_text(&info, FOL_NEW, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR);
	if (textout < 0)
		exit(-1);
	Lock_File(textout);

	printf("Copying all messages from number %d.\n", limit);
	while(read(indexin,&idx,sizeof(idx))>0)
	{
		/* if index is the start index or later, copy it */
		if (idx.Ref>=limit && !(idx.status&2) && idx.Ref <= info.last)
		{
			first=idx.Ref<first?idx.Ref:first;
			textbuff=(char *)malloc(idx.size+1);
			lseek(textin, idx.datafield, SEEK_SET);
			read(textin,textbuff,idx.size);
			idx.datafield=lseek(textout, 0, SEEK_CUR);
			write(textout,textbuff,idx.size);
			write(indexout,&idx,sizeof(idx));
			free(textbuff);
			counter++;
		}
	}
	fsync(indexout);
	Unlock_File(indexout);
	close(indexout);
	fsync(textout);
	Unlock_File(textout);
	close(textout);

	printf("Copy complete, %d messages copied.\n",counter);
	printf("First message is now #%d\n",first);
	printf("Updating Real Folder...\n");

	move_folder(&info, FOL_NEW, FOL_LIVE);

	Unlock_File(indexin);
	close(indexin);
	Unlock_File(textin);
	close(textin);

	info.first=first;
	if ((lseek(ffile,-sizeof(info),SEEK_CUR))<0)
		perror("seek");
	write(ffile,&info,sizeof(info));
	close(ffile);
}

void c_board(CommandList *cm, int argc, const char **argv, char *args)
{
	int ftmp;

	if (stringcmp(argv[1],"lock",-1))
	{
		if (access(LOCKFILE,00))
		{
			if ((ftmp=creat(LOCKFILE,00))<0)
			{
				printf(_("Could not lockboard.\n"));
				perror("Lock Board");
			}else
			{
				printf(_("Board now locked.\n"));
				close(ftmp);
				broadcast(0, "Board is now locked!");
			}
		}else
			printf(_("Already Locked.\n"));
	}else
	if (stringcmp(argv[1],"unlock",-1))
	{
		if (!access(LOCKFILE,00))
		{
			unlink(LOCKFILE);
			printf(_("Board now unlocked.\n"));
			broadcast(0, "Board has been unlocked!");
		}else
			printf(_("Board not locked.\n"));
	}else
		printf(_("Do you want to lock or unlock it.\n"));
}

void c_listusers(CommandList *cm, int argc, const char **argv, char *args)
{
	list_users(false);
}

void c_newusers(CommandList *cm, int argc, const char **argv, char *args)
{
	list_users(true);
}

void c_since(CommandList *cm, int argc, const char **argv, char *args)
{
	list_users_since(user->record.lastlogout);
}

void c_mod(CommandList *cm, int argc, const char **argv, char *args)
{
	moderate();
}

void c_passwd(CommandList *cm, int argc, const char **argv, char *args)
{
	char pw1[PASSWDSIZE], pw2[PASSWDSIZE], salt[3];

	salt[2]=0;
	strncpy(salt, user->record.passwd, 2);
	if (strcmp(user->record.passwd, crypt(get_pass(_("Enter old password: ")),salt)))
		printf(_("Incorrect.\n"));
	else
	{
		pick_salt(salt);
		strcpy(pw1,crypt(get_pass(_("New password: ")),salt));
		strcpy(pw2,crypt(get_pass(_("Again: ")),salt));
		if (strcmp(pw1,pw2))
			printf(_("Did not match.\n"));
		else
		{
			strcpy(user->record.passwd, pw1);
			update_user(user);
			printf(_("Password set.\n"));
			mwlog("CHANGE(PASSWD)");
		}
	}
}

void c_resub(CommandList *cm, int argc, const char **argv, char *args)
{
	if (get_subscribe(user, currentfolder))
		printf(_("You are already subscribed to %s.\n"), user->folder.name);
	else
	{
		set_subscribe(user, currentfolder, true);
		printf(_("Resubscribing to %s.\n"), user->folder.name);
	}
}

void c_unsub(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!get_subscribe(user, currentfolder))
		printf(_("Already Unsubscribed from %s.\n"), user->folder.name);
	else
	{
		set_subscribe(user, currentfolder, false);
		printf(_("Unsubscribing from %s.\n"), user->folder.name);
	}
}

void c_read(CommandList *cm, int argc, const char **argv, char *args)
{
	int num=atoi(argv[1]);

	if (read_msg(currentfolder, num, user)) last_mesg=num;
}

void c_prev(CommandList *cm, int argc, const char **argv, char *args)
{
	if (last_mesg <= user->folder.first)
		printf(_("You are already at the beginning of the folder.\n"));
	else if (read_msg(currentfolder, last_mesg-1, user)) last_mesg--;
}

void c_next(CommandList *cm, int argc, const char **argv, char *args)
{
	if (last_mesg >= user->folder.last)
		printf(_("You are already at the end of this folder.\n"));
	else if (read_msg(currentfolder, last_mesg+1, user)) last_mesg++;
}

void c_status(CommandList *cm, int argc, const char **argv, char *args)
{
	char stats[10],gr[10], specials[20];
	int protPower;

	show_user_stats(user->record.status, stats, false);
	show_fold_groups(user->record.groups, gr, false);
	show_special(user->record.special, specials, false);
	printf(_("Current Status of %s\n"),user->record.name);
	printf(_("Your real name is %s\n"),user->record.realname);
	printf(_("Your contact address is %s\n"),user->record.contact);
	printf(_("Your current status is [%s]\n"),stats);
	printf(_("Special settings are [%s]\n"),specials);

	protPower = (user->record.chatprivs & CP_PROTMASK) >> CP_PROTSHIFT;
	if (cm_test(user, CP_PROTECT) && protPower > 0)
	{
	    char tmp[5];
	    show_protection(user->record.chatmode, user->record.chatprivs, tmp, protPower);
	    if (!cm_test_any(user, CM_PROTMASK) && cm_test(user, CM_PROTECTED))
		    tmp[0] = 'p';
	    printf(_("Talker modes=[%s] privs=[%s] protection=[%s]\n"),
		   display_cmflags(user->record.chatmode & ~CM_PROTECTED),
		   display_cpflags(user->record.chatprivs & ~CP_PROTECT),
		   tmp);
	} else
	{
	    printf(_("Talker modes=[%s] privs=[%s]\n"),
	           display_cmflags(user->record.chatmode),
	           display_cpflags(user->record.chatprivs));
	}
	printf(_("You have set messages %s.\n"), u_mesg(user) ? _("off") : _("on"));
	printf(_("You will%sbe informed of logins and logouts\n"), u_inform(user) ? _(" not ") : " ");
	printf(_("You will%shear beeps.\n"), u_beep(user) ? _(" not ") : " ");
	if (u_god(user) || s_wizchat(user))
	{
		printf(_("You can use wizchat"));
		if (s_chatoff(user))
			printf(_(", but you will not hear any replies"));
		printf(".\n");
	}
	if (u_god(user) || s_changeinfo(user))
		printf(_("You %s informed of user status changes.\n"),
		       s_changeinfo(user)?_("will"):_("will not"));
	if (*gr) printf(_("You belong to the following group(s) [%s]\n"),gr);
	printf(_("You are currently in folder %s, which you "), user->folder.name);
	printf(get_subscribe(user, currentfolder)?_("are"):_("are not"));
	printf(_(" subscribed to.\n"));
	if (user->record.timeout == 0)
		printf(_("You will not be timed out for being idle.\n"));
	else
		printf(_("You will be timed out after being idle for %s.\n"),itime(user->record.timeout));
	time_on(user->record.timeused+(time(0) - user->loggedin));
}

static void credits(void)
{
	printf("The Milliways III bulletin board system.\n");
	printf("(c) 1992 Arthur Dent (J.S.Mitchell)\n");
	printf("\nLinux / Internet patches: Anarchy\n");
	printf("Basis of new command parser: Anarchy\n");
	printf("General Thanks to:\n");
	printf("Arashi (ideas), Hobbit (spellings), Madsysop (for being annoying),\n");
	printf("Fry (all those useful things that everyone seems to use, and MUD rooms),\n");
	printf("Finnw (I/O multiplexing, dynamic hash tables),\n");
	printf("FireFury (debugging code, IPC abstraction),\n");
	printf("Cmckenna (MUD rooms, gag filters, help text updates, ideas (occasionally good ones), bug finding),\n");
	printf("Dez (man page style help, ideas, testing),\n");
	printf("Pwb (RPM packaging, UTF-8 compatibility hacks),\n");
	printf("Psycodom (Fixups for IPC, JS, locale conversions, buffer overflows)\n");
	printf("welshbyte (Code refactoring, tidying and general cleanups)\n");
	printf("\n");
}

void c_credits(CommandList *cm, int argc, const char **argv, char *args)
{
	credits();
}

void c_write(CommandList *cm, int argc, const char **argv, char *args)
{
	busy++;
	add_msg(currentfolder, user, 0);
	busy--;
}

void c_reply(CommandList *cm, int argc, const char **argv, char *args)
{
	if (last_mesg==0)
		printf(_("You have to read a message before you can reply to it.\n"));
	else
	{
		printf(_("Replying to message %d.\n"),last_mesg);
		busy++;
		add_msg(currentfolder, user, last_mesg);
		busy--;
	}
}

void c_catchup(CommandList *cm, int argc, const char **argv, char *args)
{
	int i,n;
	struct folder f;

	if (nofolders())
	{
		printf(_("No folders found.\n"));
		return;
	}
	if (argc>1 && !strcasecmp(argv[1], "all"))
	{
		printf(_("Marking ALL folders as read.\n"));
		if ((n=openfolderfile(O_RDONLY))<0)
		{
			printf("Error: %s\n",strerror(errno));
			return;
		}
		i=0;
		while (get_folder_entry(n, &f)>0)
		{
			if (f_active(f.status))
				user->record.lastread[i] = f.last;
			i++;
		}
		close(n);
		return;
	}else
	if (argc>1)
	{
		struct folder *fold = &user->folder;

		if ((i=foldernumber(argv[1]))<0)
		{
			printf(_("There is no folder '%s'\n"),argv[1]);
			return;
		}
		get_folder_number(fold, i);
		if(!f_active(fold->status) || (!allowed_r(fold,user) && !allowed_w(fold,user)))
		{
			printf(_("There is no folder %s\n"), argv[1]);
			get_folder_number(fold,currentfolder);
			return;
		}
	}else
	{
		get_folder_number(&user->folder, currentfolder);
		i=currentfolder;
	}
	user->record.lastread[i] = user->folder.last;
	update_user(user);
	printf(_("Marking folder %s as read.\n"), user->folder.name);
	get_folder_number(&user->folder, currentfolder);
}

void c_date(CommandList *cm, int argc, const char **argv, char *args)
{
	long tm;
	tm=time(0);
	printf(_("Current time and date is %s"),ctime(&tm));
}

void c_quit(CommandList *cm, int argc, const char **argv, char *args)
{
	close_down(0, NULL, NULL);
}

void c_save(CommandList *cm, int argc, const char **argv, char *args)
{
	int msg=atoi(argv[1]);
	char buff[128],file[128];
	FILE *f;

	int i,len,x;

	len=strlen(argv[2]);
	for(i=0;i<len;i++)
	{
		if (argv[2][i]=='/') file[i]='_';
		else
			file[i]=argv[2][i];
	}
	file[len]=0;

	sprintf(buff,"/tmp/%s",file);

	x=fork();
	if (x==-1)
	{
		printf("fork() failed, cannot save.\n");
	}else
	if (x==0)
	{
		/* we are child */
		if (perms_drop()==-1) {perror("setuid");exit(0);}
		if ((f=fopen(buff,"a"))==NULL)
		{
			perror(file);
			exit(0);
		}
		output=f;
		fprintf(output,"Saved Message:\n");
		perms_restore();
		read_msg(currentfolder, msg, user);
		fclose(output);
		printf("Mesg no %d saved to %s\n",msg,buff);
		output=stdout;
		exit(0);
	}else
	{
		do{
			x=wait(NULL);
		}while (x==-1 && errno==EINTR);
		if (x==-1)
			perror("wait");
	}
}

void c_timeout(CommandList *cm, int argc, const char **argv, char *args)
{
	int z;
	int units=1;
	char *s = NULL;
	z=strtol(argv[1], &s, 10);

	/* no number found */
	if (s == argv[1]) {
		printf(_("TIMEOUT must be at least 10 minutes.\n"));
		return;
	}

	if (*s == '\0' || strchr("sS", *s))
		units = 1;
	else
	if (strchr("mM",*s))
		units = 60;
	else
	if (strchr("hH",*s))
		units = 3600;
	else
	if (strchr("dD",*s))
		units = 86400;
	else {
		printf(_("Invalid time unit '%c' must be one of: dhms.\n"), *s);
		return;
	}

	z *= units;

	if (z<600)
	{
		printf(_("TIMEOUT must be at least 10 minutes.\n"));
	}else
	{
		if (user->record.timeout != z)
		{
			user->record.timeout = z;
			printf(_("TIMEOUT now set to %s\n"), itime(user->record.timeout));
		}else
			printf(_("TIMEOUT was already set to %s.\n"), itime(user->record.timeout));
	}

	update_user(user);
	reset_timeout(user->record.timeout);
}

void c_timestamp(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (s_timestamp(user))
		{
			user->record.special &= ~(1 << MWSPCL_TIMESTAMP);
			printf(_("Timestamps now disabled.\n"));
		}else
			printf(_("Timestamping was already off.\n"));
	}else
	if (z==1)
	{
		if (!s_timestamp(user))
		{
			user->record.special |= (1 << MWSPCL_TIMESTAMP);
			printf(_("Timestamps now enabled.\n"));
		}else
			printf(_("Timestamping already enabled.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_postinfo(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (s_postinfo(user))
		{
			user->record.special &= ~(1 << MWSPCL_POSTINFO);
			printf(_("Posting information now suppressed.\n"));
		}else
			printf(_("Posting info was already off.\n"));
	}else
	if (z==1)
	{
		if (!s_postinfo(user))
		{
			user->record.special |= (1 << MWSPCL_POSTINFO);
			printf(_("You will now be informed of new postings.\n"));
		}else
			printf(_("Posting info was already on.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_changeinfo(CommandList *cm, int argc, const char **argv, char *args)
{
	int z=BoolOpt(argv[1]);
	if (z==0)
	{
		if (s_changeinfo(user))
		{
			user->record.special &= ~(1 << MWSPCL_CHGINFO);
			printf(_("User change information now suppressed.\n"));
		}else
			printf(_("Change info was already off.\n"));
	}else
	if (z==1)
	{
		if (!s_changeinfo(user))
		{
			user->record.special |= (1 << MWSPCL_CHGINFO);
			printf(_("You will now be informed of user status changes.\n"));
		}else
			printf(_("Change info was already on.\n"));
	}else
		printf("%s\n",cm->ArgError);
	update_user(user);
}

void c_chatmode(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!cm_test(user, CM_ONCHAT))
		t_chaton();
	else
		printf(_("Already in chat mode, silly.\n"));
	update_user(user);
}

void c_contact(CommandList *cm, int argc, const char **argv, char *args)
{
	/* user is allowed to change their contact address */
	if (!s_fixedcontact(user))
	{
		busy++;
		edit_contact();
		busy--;
	}
	/* user is not allowed to change their contact */
	else
	{
		printf(_("You are not allowed to change your contact address. Please notify a SuperUser to change this for you.\n"));
	}
}

void c_alias(CommandList *cm, int argc, const char **argv, char *args)
{
	if (AddLink(&alias_list, argv[1], argv[2]))
		printf(_("Alias '%s' already exists. Has now been redefined!\n"), argv[1]);
	else
		printf(_("Alias '%s' added!\n"), argv[1]);
}

void c_unalias(CommandList *cm, int argc, const char **argv, char *args)
{
	if (!strcasecmp("*", argv[1]))
	{
		DestroyAllLinks(&alias_list);
		printf(_("All Aliases Destroyed!\n"));
	}
	else
	{
		if (!DestroyLink(&alias_list, argv[1]))
			printf(_("Alias '%s' was not found!\n"), argv[1]);
		else
			printf(_("Alias '%s' was destroyed...\n"), argv[1]);
	}
}

void c_charset(CommandList *cm, int argc, const char **argv, char *args)
{
    if (argc == 1) {
	/* XXX: gettextify this */
	printf("Local character set is currently %s\n",
		get_local_charset());
    } else {
	(void)set_local_charset(argv[1]);
    }
}

void c_locale(CommandList *cm, int argc, const char **argv, char *args)
{
    if (argc == 1) {
	/* XXX: gettextify this */
	printf("Locale is currently %s\n",
		setlocale(LC_CTYPE, NULL));
    } else {
	(void)locale_mesgs(argv[1]);
    }
}
