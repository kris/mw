#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <pwd.h>
#include <errno.h>
#include <jansson.h>
#include <curl/curl.h>
#include <readline/readline.h>
#include <duktape.h>

#include <sqlite.h>
#include "js.h"
#include "main.h"
#include "script.h"
#include "talker.h"
#include "chattable.h"
#include "alias.h"
#include "user.h"
#include "alarm.h"
#include "who.h"
#include "util.h"
#include "iconv.h"
#include "sqlite.h"
#include "init.h"

extern struct user * const user;
struct alarm *timeout_event = NULL;
int interrupt = 1;

duk_context *ctx;

struct binding {
	int type;
	int name_required;
	alias **list;
	const char *name;
};

const struct binding bindings[] = {
	{ K_BIND, 1, &bind_list, "Bind" },
	{ K_BIND_ALIAS, 1, &alias_list, "Alias" },
	{ K_BIND_RPC, 1, &rpc_list, "RPC bind" },
	{ K_BIND_EVENT, 0, &event_list, "Event bind" },
	{ K_BIND_ONOFF, 0, &onoff_list, "Check on/off bind" },
	{ K_BIND_IPC, 0, &ipc_list, "IPC bind" },
	{ K_BIND_FORCE, 0, &force_list, "Force bind" },
	{ K_BIND_SHUTDOWN, 0, &shutdown_list, "Shutdown bind" },
	{ K_BIND_INPUT, 0, &eventin_list, "Input event bind" },
	{ 0, 0, NULL, NULL }
};

/* Used by duktape to check for a timeout */
duk_bool_t check_exec_timeout(void *udata)
{
	return interrupt;
}

/* called if a script runs too long */
static void timeout(void *ptr)
{
	interrupt = 2;
}

/* clears the timeout event when a js finishes (or the js uses input) */
static void clear_timeout(void)
{
	if (timeout_event != NULL) {
		timeout_event->how = NULL;
		timeout_event = NULL;
	}
}

/* starts a 3 second timer that will interupt js if it is exceeded */
static void start_timeout(void)
{
	clear_timeout();
	alarm_enable();
	timeout_event = alarm_after(3, 0, NULL, &timeout);
}

/* Duktape uses a CESU-8 encoding, which allows UTF-16 surrogate pairs
   (themselves encoded in UTF-8), in order to be kinda-sorta compatible with
   ecmascript's UTF-16 requirements. This function just copies the cesu8 string,
   converting any surrogate pairs it finds to UTF-8. */
static char *cesu8_to_utf8(const char *cesu8)
{
	char *utf8 = calloc(1, strlen(cesu8) + 1);
	const unsigned char *cc = (void *)cesu8;
	char *cu = utf8;
	uint32_t hs = 0;

	while (*cc != '\0') {
		uint32_t c = 0;
		uint32_t u;

		if (cc[0] <= 0x7F) {
			*cu++ = *cc++;
			continue;
		} else if (cc[0] <= 0xDF) {
			*cu++ = *cc++;
			*cu++ = *cc++;
			continue;
		} else if (cc[0] <= 0xEF) {
			/* Surrogates are encoded in 3 chars so convert
			   back to a single UTF-16 value */
			c = ((uint32_t)cc[0] & 0xF) << 12 |
			    ((uint32_t)cc[1] & 0x3F) << 6 |
			    ((uint32_t)cc[2] & 0x3F);
		} else {
			*cu++ = *cc++;
			*cu++ = *cc++;
			*cu++ = *cc++;
			*cu++ = *cc++;
			continue;
		}
		if (hs == 0 && c >= 0xD800 && c <= 0xDBFF)
			hs = c;
		else if (hs != 0 && c >= 0xDC00 && c <= 0xDFFF) {
			/* Have high and low surrogates - convert to code point then
			   back to UTF-8 */
			u = 0x10000 + ((((uint32_t)hs & 0x3FF) << 10) | (c & 0x3FF));
			*cu++ = 0xF0 |  u >> 18;
			*cu++ = 0x80 | (u >> 12 & 0x3F);
			*cu++ = 0x80 | (u >> 6 & 0x3F);
			*cu++ = 0x80 | (u & 0x3F);
			hs = 0;
		} else {
			*cu++ = cc[0];
			*cu++ = cc[1];
			*cu++ = cc[2];
			hs = 0;
		}
		cc += 3;
	}
	*cu = '\0';
	return utf8;
}

static duk_ret_t js_print(duk_context *cx)
{
	int argc = duk_get_top(cx);

	if (argc < 1)
		return 0;
	for (int i = 0; i < argc; i++) {
		const char *cesu8 = duk_to_string(cx, i - argc);
		char *utf8 = cesu8_to_utf8(cesu8);
		display_message(utf8, 0, 1);
		free(utf8);
	}
	return 0;
}

static duk_ret_t js_mwexec(duk_context *cx)
{
	char *utf8;

	if (duk_is_undefined(cx, -1)) {
		fprintf(stderr, "mwjs error: exec() requires an argument\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (!duk_is_string(cx, -1)) {
		fprintf(stderr, "mwjs error: exec() requires a string\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	utf8 = cesu8_to_utf8(duk_get_string(cx, -1));
	duk_pop(cx);
	DoCommand(utf8, chattable);
	free(utf8);
	return 0;
}

static duk_ret_t js_say(duk_context *cx)
{
	char *utf8;

	if (duk_is_undefined(cx, -1)) {
		fprintf(stderr, "mwjs error: say() requires an argument\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	flood++;
	if (flood > flood_limit) {
		fprintf(stderr, "mwjs error: Script exceeded flood limit\n");
		return DUK_RET_ERROR;
	}
	if (!duk_check_type_mask(cx, -1, DUK_TYPE_MASK_STRING | DUK_TYPE_MASK_NUMBER)) {
		fprintf(stderr, "mwjs error: say() takes a string or a number\n");
		return DUK_RET_ERROR;
	}
	utf8 = cesu8_to_utf8(duk_to_string(cx, -1));
	chat_say(utf8);
	duk_pop(cx);
	free(utf8);
	return 0;
}

static duk_ret_t js_wholist(duk_context *cx)
{
	duk_idx_t arr_idx;
	json_t * wlist;
	json_t *entry;
	time_t now;
	size_t wi;
	int i = 0;

	wlist = grab_wholist();
	if (wlist == NULL) {
		fprintf(stderr, "Could not grab who list, try again\n");
		return 0;
	}
	arr_idx = duk_push_array(cx);
	now = time(0);
	json_array_foreach(wlist, wi, entry) {
		json_t * perms = json_object_get(entry, "perms");
		duk_idx_t obj_idx;
		const char *prot;
		int dowhen;

		obj_idx = duk_push_bare_object(cx);
		/* user name */
		duk_push_string(cx, json_getstring(entry, "name"));
		duk_put_prop_string(cx, obj_idx, "username");
		/* room number */
		duk_push_int(cx, json_getint(entry, "channel"));
		duk_put_prop_string(cx, obj_idx, "room");
		/* idle time */
		duk_push_int(cx, now - json_getint(entry, "idletime"));
		duk_put_prop_string(cx, obj_idx, "idle");
		/* chat modes */
		duk_push_string(cx, json_getstring(perms, "chatmode"));
		duk_put_prop_string(cx, obj_idx, "chatmodes");
		/* protection level */
		prot = json_getstring(perms, "protection");
		duk_push_int(cx, prot[0] - '0');
		duk_put_prop_string(cx, obj_idx, "protection_level");
		duk_push_int(cx, prot[2] - '0');
		duk_put_prop_string(cx, obj_idx, "protection_power");
		/* chat perms */
		duk_push_string(cx, json_getstring(perms, "chatprivs"));
		duk_put_prop_string(cx, obj_idx, "chatprivs");
		/* status string */
		duk_push_string(cx, json_getstring(entry, "doing"));
		duk_put_prop_string(cx, obj_idx, "doing");
		dowhen = json_getint(entry, "dowhen");
		duk_push_int(cx, dowhen ? now - dowhen : 0);
		duk_put_prop_string(cx, obj_idx, "since");
		/* stick line into array */
		duk_put_prop_index(ctx, arr_idx, i++);
	}
	return 1; /* Array is returned at top of stack */
}

static duk_ret_t js_rpc(duk_context *cx)
{
	const char *username = NULL;
	const char *rpc_type = NULL;
	const char *msg = NULL;
	int broadcast = 0;

	if (duk_is_undefined(cx, -1) ||
	    duk_is_undefined(cx, -2) ||
	    duk_is_undefined(cx, -3)) {
		fprintf(stderr, "mwjs error: rpc() expects 3 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (duk_is_number(cx, -3)) {
		if (duk_get_int(cx, -3) == K_BROADCAST)
			broadcast = 1;
	} else if (duk_is_string(cx, -3)) {
		username = duk_get_string(cx, -3);
	}
	rpc_type = duk_safe_to_string(cx, -2);
	msg = duk_safe_to_string(cx, -1);

	if((!broadcast && username[0] == '\0') || rpc_type[0] == '\0') {
		fprintf(stderr, "Error: javascript rpc(): invalid arguments - [%s] [%s]",
		        (broadcast ? "K_BROADCAST" : username), rpc_type);
		return DUK_RET_ERROR;
	}
	sendrpc(username, rpc_type, msg, broadcast);
	return 0;
}

static duk_ret_t js_ipc(duk_context *cx)
{
	const char *username = NULL;
	const char *msg = NULL;
	int broadcast = 0;

	if (duk_is_undefined(cx, -1) || duk_is_undefined(cx, -2)) {
		fprintf(stderr, "mwjs error: ipc() expects 2 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (duk_is_number(cx, -2)) {
		if (duk_get_int(cx, -2) == K_BROADCAST)
			broadcast = 1;
	} else if (duk_is_string(cx, -2)) {
		username = duk_get_string(cx, -2);
	}
	msg = duk_safe_to_string(cx, -1);
	if (broadcast == 0 && (!username || username[0] == '\0')) {
		fprintf(stderr, "mwjs error: ipc() expects either a username or K_BROADCAST\n");
		return DUK_RET_ERROR;
	}
	sendipc(username, msg, broadcast);
	return 0;
}

struct urlget {
	duk_context *cx;
	duk_idx_t nchunks;
};
/* Consume data chunk acquired by curl */
static size_t omnomnom(void *ptr, size_t size, size_t n, void *data)
{
	struct urlget *ug = data;
	if (data == NULL)
		return 0;
	duk_push_lstring(ug->cx, ptr, size * n);
	ug->nchunks++;
	return size * n;
}

static duk_ret_t js_urlget(duk_context *cx)
{
	struct urlget ug = { .cx = cx, .nchunks = 0 };
	char cerr[CURL_ERROR_SIZE];
	const char *url;
	CURL *cl;

	if (duk_is_undefined(cx, -1)) {
		fprintf(stderr, "mwjs error: urlget() expects an argument\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (!duk_is_string(cx, -1)) {
		fprintf(stderr, "mwjs error: urlget() requires a string\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	url = duk_get_string(cx, -1);
	cl = curl_easy_init();
	curl_easy_setopt(cl, CURLOPT_WRITEFUNCTION, omnomnom);
	curl_easy_setopt(cl, CURLOPT_WRITEDATA, &ug);
	curl_easy_setopt(cl, CURLOPT_URL, url);
	curl_easy_setopt(cl, CURLOPT_ERRORBUFFER, cerr);
	curl_easy_setopt(cl, CURLOPT_USERAGENT, "Milliways III v" VERSION);
	if (curl_easy_perform(cl)) {
		fprintf(stderr, "mwjs error: urlget(): '%s': '%s'\n", url, cerr);
		return DUK_RET_ERROR;
	}
	curl_easy_cleanup(cl);
	/* Join the chunks together */
	duk_concat(cx, ug.nchunks);
	return 1; /* Result is at the top of the stack */
}

static duk_ret_t js_beep(duk_context *cx)
{
	int i, beeps;

	beeps = duk_get_int_default(cx, -1, 1);
	if (beeps < 1 || beeps > 5) {
		beeps = 0;
		fprintf(stderr, "beep() will only do between 1 and 5 beeps\n");
	}
	for (i = 0; i < beeps; i++)
		write(STDOUT_FILENO, "\7", 1);
	return 0;
}

/* Caller must free the returned memory */
static char *prompt_to_local(duk_context *cx)
{
	_autofree char *instr = NULL;
	const char *dukstr;
	duk_size_t dukbufsize;
	size_t promptbufsize;
	char *prompt;
	size_t len;
	int err;

	len = duk_get_length(cx, -1);
	dukstr = duk_get_lstring(cx, -1, &dukbufsize);
	promptbufsize = sizeof(char) * ((len * 3) + 1); // Still ugly
	prompt = malloc(promptbufsize); // Freed by caller
	duk_pop(cx);

	if (dukstr == NULL)
		return strdup("? ");

	/* To retain const correctness we have to dup the js string
	   which might seem slow but prompts will usually be short
	   so it's not worth worrying about */
	instr = strdup(dukstr);
	err = convert_string_charset(instr, "UTF-8", dukbufsize,
	                             prompt, "LOCAL//TRANSLIT", promptbufsize,
	                             NULL, NULL, NULL, NULL, NULL);
	if (err < 0) {
		fprintf(stderr, "mwsjs error: input() failed to convert "
				"prompt string: %d\n", err);
	}
	if (prompt == NULL)
		return strdup("? ");
	return prompt;
}

static duk_ret_t js_input(duk_context *cx)
{
	_autofree char *prompt = NULL;
	_autofree char *outstr = NULL;
	_autofree char *line = NULL;
	size_t len;
	int err;

	if (!duk_is_undefined(cx, -1) && !duk_is_string(cx, -1)) {
		fprintf(stderr, "mwjs error: argument to input() must be a string\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	prompt = prompt_to_local(cx);

	busy++;
	clear_timeout();
	line = readline(prompt);
	start_timeout();
	busy--;

	if (line == NULL) {
		duk_push_string(cx, "");
		return 1;
	}
	len = strlen(line) * 3 + 3; // Also ugly
	outstr = malloc(len);
	err = convert_string_charset(
	                line, "LOCAL", strlen(line),
                        outstr, "UTF-8//TRANSLIT", len,
                        NULL, NULL, NULL, NULL, NULL);
	if (err < 0) {
		fprintf(stderr, "mwjs error: input(): garbled string\n");
		return DUK_RET_ERROR;
	}
	duk_push_string(cx, outstr);
	return 1;
}

static duk_ret_t js_termsize(duk_context *cx)
{
	int idx;

	idx = duk_push_bare_object(cx);
	duk_push_int(cx, screen_w());
	duk_put_prop_string(cx, idx, "width");
	duk_push_int(cx, screen_h());
	duk_put_prop_string(cx, idx, "height");
	return 1; /* Result is at top of stack */
}

static duk_ret_t js_bind(duk_context *cx)
{
	const char *bind_name = NULL;
	const char *func_name = NULL;
	const struct binding *bind;
	int bind_type = -1;

	if (duk_is_undefined(cx, -2) ||
	    duk_is_undefined(cx, -3)) {
		fprintf(stderr, "mwjs error: bind() expects 2 or 3 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (duk_is_string(cx, -3)) {
		bind_name = duk_get_string(cx, -3);
		bind_type = K_BIND;
		if (!duk_is_undefined(cx, -1)) {
			fprintf(stderr, "mwjs error: bind(str, str) expects 2 arguments\n");
			return DUK_RET_SYNTAX_ERROR;
		}
		duk_pop(cx);
	} else if (duk_is_number(cx, -3)) {
		bind_type = duk_get_int_default(cx, -3, -1);
		if (bind_type == K_BIND ||
		    bind_type == K_BIND_ALIAS ||
		    bind_type == K_BIND_RPC) {
			bind_name = duk_get_string(cx, -2);
		} else {
			duk_pop(cx);
		}
	} else {
		fprintf(stderr, "mwjs error: first argument to bind() must be a "
		                "string or bind id\n");
		return DUK_RET_SYNTAX_ERROR;
	}

	func_name = duk_get_string(cx, -1);
	if (func_name == NULL) {
		fprintf(stderr, "mwjs error: bind(): invalid function name\n");
		return DUK_RET_SYNTAX_ERROR;
	}

	for (bind = &bindings[0]; bind->list != NULL && bind->type != bind_type; bind++);

	if (bind->list == NULL) {
		fprintf(stderr, "Bind type %d not recognised\n", bind_type);
		return DUK_RET_ERROR;
	}
	if (bind->name_required) {
		if (bind_name == NULL || bind_name[0] == '\0') {
			fprintf(stderr, "Bind name is empty\n");
			return DUK_RET_ERROR;
		}
	} else {
		bind_name = func_name;
		func_name = "";
	}
	if (AddLink(bind->list, bind_name, func_name)) {
		fprintf(stderr, "Warning: %s %s already exists.\n",
		        bind->name, bind_name);
	}
	return 0;
}

static duk_ret_t js_unbind(duk_context *cx)
{
	const char *bind_name = "";
	const struct binding *bind;
	int bind_type = -1;

	if (duk_is_undefined(cx, -2)) {
		fprintf(stderr, "mwjs error: unbind() expects 1 or 2 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (duk_is_string(cx, -2)) {
		bind_name = duk_get_string(cx, -2);
		bind_type = K_BIND;
		if (!duk_is_undefined(cx, -1)) {
			fprintf(stderr, "mwjs error: unbind(str) expects 1 argument\n");
			return DUK_RET_SYNTAX_ERROR;
		}
		duk_pop(cx);
	} else if (duk_is_number(cx, -2)) {
		bind_type = duk_get_int_default(cx, -2, -1);
		bind_name = duk_get_string(cx, -1);
	} else {
		fprintf(stderr, "mwjs error: first argument to unbind() must be a "
		                "string or bind id\n");
		return DUK_RET_SYNTAX_ERROR;
	}

	for (bind = &bindings[0]; bind->list != NULL && bind->type != bind_type; bind++);

	if (bind->list == NULL) {
		fprintf(stderr, "Bind type %d not recognised\n", bind_type);
		return DUK_RET_ERROR;
	}
	if (bind->name_required) {
		if (bind_name == NULL || bind_name[0] == '\0') {
			fprintf(stderr, "Bind name is empty\n");
			return DUK_RET_ERROR;
		}
	}
	if (!DestroyLink(bind->list, bind_name)) {
		fprintf(stderr, "Warning: %s %s does not exist\n",
		        bind->name, bind_name);
	}
	return 0;
}

static void js_push_result_entry(duk_context *cx, struct db_data *entry, int ncols)
{
	duk_idx_t idx;
	int i;

	if (entry == NULL || ncols < 1) {
		duk_push_null(cx);
		return;
	}
	idx = duk_push_array(cx);
	for (i = 0; i < ncols; i++) {
		duk_push_string(cx, entry->field[i]);
		duk_put_prop_index(cx, idx, i);
	}
}

static void js_push_result_array(duk_context *cx, struct db_result *dbres)
{
	struct db_data *entry;
	duk_idx_t idx;
	int i;

	if (dbres == NULL) {
		duk_push_null(cx);
		return;
	}
	idx = duk_push_array(cx);
	for (i = 0, entry = dbres->data; entry != NULL; entry = entry->next, i++) {
		js_push_result_entry(cx, entry, dbres->cols);
		duk_put_prop_index(cx, idx, i);
	}
}

static void js_push_column_names(duk_context *cx, struct db_result *dbres)
{
	duk_idx_t idx;
	int i;

	if (dbres == NULL) {
		duk_push_null(cx);
		return;
	}
	idx = duk_push_array(cx);
	for (i = 0; i < dbres->cols; i++) {
		duk_push_string(cx, dbres->colNames[i]);
		duk_put_prop_index(cx, idx, i);
	}
}

static void js_push_dbresult(duk_context *cx, struct js_db_result *dbres)
{
	duk_idx_t idx = duk_push_bare_object(cx);
	duk_push_int(cx, dbres->db_error);
	duk_put_prop_string(cx, idx, "db_error");
	duk_push_int(cx, dbres->query_error);
	duk_put_prop_string(cx, idx, "query_error");
	if (dbres->error_text != NULL)
		duk_push_string(cx, dbres->error_text);
	else
		duk_push_string(cx, "No Error");
	duk_put_prop_string(cx, idx, "error_text");
	js_push_result_array(cx, dbres->query_result);
	duk_put_prop_string(cx, idx, "data");
	js_push_column_names(cx, dbres->query_result);
	duk_put_prop_string(cx, idx, "column_names");
}

static duk_ret_t js_dbquery(duk_context *cx)
{
	_autofree char *path = NULL;
	struct js_db_result *dbres;
	const char *dbname;
	const char *query;
	struct passwd *pw;

	if ((pw = getpwuid(getuid())) == NULL) {
		fprintf(stderr, "mwjs error: dbquery(): Error getting user information\n");
		return DUK_RET_ERROR;
	}
	if (getmylogin() == NULL) {
		fprintf(stderr, "mwjs error: dbquery(): Permission denied\n");
		return DUK_RET_ERROR;
	}
	if (duk_is_undefined(cx, -1) ||
	    duk_is_undefined(cx, -2)) {
		fprintf(stderr, "mwjs error: dbquery() requires 2 arguments\n");
		return DUK_RET_SYNTAX_ERROR;
	}
	if (!(duk_is_string(cx, -1) && duk_is_string(cx, -2))) {
		fprintf(stderr, "mwjs error: dbquery() requires 2 strings\n");
		return DUK_RET_ERROR;
	}
	dbname = duk_get_string(cx, -2);
	query = duk_get_string(cx, -1);
	if (!dbname || dbname[0] == '/' || !strncmp(dbname, "../", 3) ||
	    strstr(dbname, "/../")) {
		fprintf(stderr, "mwjs error: dbquery(): illegal path '%s'\n", dbname);
		return DUK_RET_ERROR;
	}
	asprintf(&path, "%s/%s", pw->pw_dir, dbname);
	perms_drop();
	dbres = js_db_query(path, query);
	perms_restore();
	if (!dbres) {
		fprintf(stderr, "mwjs error: dbquery(): query failed\n");
		return DUK_RET_ERROR;
        }
	js_push_dbresult(cx, dbres);
	js_db_free(dbres);
	return 1;
}


int js_isrunning(void)
{
	return (interrupt == 0 && timeout_event != NULL);
}

int js_exec(char *name, int argc, const char **argv)
{
	duk_int_t ret;
	int i;

	if (!duk_get_global_string(ctx, name) || !duk_is_function(ctx, -1))
		return 0;
	for (i = 0; i < argc; i++)
		duk_push_string(ctx, argv[i]);

	interrupt = 0;
	start_timeout();
	ret = duk_pcall(ctx, argc);
	clear_timeout();

	if (ret != DUK_EXEC_SUCCESS) {
		if (duk_is_error(ctx, -1)) {
			duk_get_prop_string(ctx, -1, "stack");
			fprintf(stderr, "mwjs error: %s\n", duk_safe_to_string(ctx, -1));
			duk_pop(ctx);
		} else {
			fprintf(stderr, "mwjs error: %s\n", duk_safe_to_string(ctx, -1));
		}
		script_output = 0;
	} else if (duk_is_boolean(ctx, -1) && !duk_get_boolean(ctx, -1)) {
		script_output = 0;
	}
	duk_pop(ctx);
	return 0;
}

/* This limit is fairly arbitrary but a limit is needed and 256K
   seems a reasonable file and (temporary) buffer size */
#define MWJS_FILE_MAX_SIZE (256*1024)

int load_jsfile(FILE *f, const char *filename)
{
	char buf[MWJS_FILE_MAX_SIZE + 1];
	int fd = fileno(f);
	struct stat st;
	size_t ret;
	int err;

	if (fd == -1) {
		fprintf(stderr, "Failed to open '%s': %s\n", filename, strerror(errno));
		return 1;
	}
	if (fstat(fd, &st) != 0) {
		fprintf(stderr, "Failed to stat '%s': %s\n", filename, strerror(errno));
		return 1;
	}
	errno = EFBIG;
	if (st.st_size > MWJS_FILE_MAX_SIZE) {
		fprintf(stderr, "Failed to load '%s': %s\n", filename, strerror(errno));
		return 1;
	}
	ret = fread(buf, sizeof(char), st.st_size, f);
	if (ret != st.st_size) {
		fprintf(stderr, "Failed to read '%s': %llu bytes read\n",
		        filename, (unsigned long long)ret);
		return 1;
	}
	buf[st.st_size] = '\0';

	interrupt = 0;
	start_timeout();
	duk_push_string(ctx, filename);
	err = duk_pcompile_string_filename(ctx, 0, buf);
	if (err == 0)
		err = duk_pcall(ctx, 0);
	clear_timeout();

	if (err != 0)
		fprintf(stderr, "Failed to execute '%s': %s\n", filename,
		        duk_safe_to_string(ctx, -1));
	duk_pop(ctx);
	return 0;
}

int is_js(char *name)
{
	int ret = 0;

	if (duk_get_global_string(ctx, name) && duk_is_function(ctx, -1))
		ret = 1;

	duk_pop(ctx);
	return ret;
}

void js_stop_execution(void)
{
	interrupt = 1;
}

int stop_js(void)
{
	duk_destroy_heap(ctx);
	return 0;
}

static duk_ret_t js_store_get(duk_context *cx)
{
	const char *key = duk_get_string(cx, 1);
	char *val = userdb_get(USERDB_PUBLIC, user->record.name, key);
	if (val == NULL)
		duk_push_undefined(cx);
	else
		duk_push_string(cx, val);
	return 1;
}

static duk_ret_t js_store_set(duk_context *cx)
{
	const char *key = duk_get_string(cx, 1);
	const char *val = duk_get_string(cx, 2);
	userdb_set(USERDB_PUBLIC, user->record.name, key, val);
	return 0;
}

static const duk_function_list_entry store_handlers[] = {
	{ "get", js_store_get, 3 },
	{ "set", js_store_set, 4 },
	{ NULL, NULL, 0 }
};

/* The owning object must be at the top of the stack when calling this */
static void define_store_object(void)
{
	duk_push_string(ctx, "Store"); /* To be used by duk_def_prop() below */

	duk_get_prop_string(ctx, -2, "Proxy"); /* Push the built-in Proxy constructor */
	duk_push_object(ctx); /* Store */
	duk_push_object(ctx); /* Handler object */
	duk_put_function_list(ctx, -1, store_handlers);
	duk_new(ctx, 2); /* js equiv: new Proxy(Store, handler); */

	/* Define the Store object in the global object */
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_func(const char *name, duk_c_function func, int nargs)
{
	duk_push_string(ctx, name);
	duk_push_c_function(ctx, func, nargs);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_number(const char *name, duk_double_t num)
{
	duk_push_string(ctx, name);
	duk_push_number(ctx, num);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_string(const char *name, const char *value)
{
	duk_push_string(ctx, name);
	duk_push_string(ctx, value);
	duk_def_prop(ctx, -3, DUK_DEFPROP_HAVE_VALUE);
}

/* The owning object must be at the top of the stack when calling this */
static void define_constants(void)
{
	define_number("K_BIND_EVENT", (duk_double_t)K_BIND_EVENT);
	define_number("K_BIND_IPC", (duk_double_t)K_BIND_IPC);
	define_number("K_BIND_ONOFF", (duk_double_t)K_BIND_ONOFF);
	define_number("K_BIND_FORCE", (duk_double_t)K_BIND_FORCE);
	define_number("K_BIND_SHUTDOWN", (duk_double_t)K_BIND_SHUTDOWN);
	define_number("K_BIND_RPC", (duk_double_t)K_BIND_RPC);
	define_number("K_BIND_ALIAS", (duk_double_t)K_BIND_ALIAS);
	define_number("K_BIND_INPUT", (duk_double_t)K_BIND_INPUT);
	define_number("K_BROADCAST", (duk_double_t)K_BROADCAST);
	define_string("whoami", user->record.name);
}

int setup_js(void)
{
	int is_local = 1;

	if (getmylogin() == NULL)
		is_local = 0;

	ctx = duk_create_heap_default();
	if (ctx == NULL)
		return -1;

	duk_push_global_object(ctx);
	define_constants();
	define_func("print", js_print, DUK_VARARGS);
	define_func("exec", js_mwexec, 1);
	define_func("say", js_say, 1);
	define_func("wholist", js_wholist, 0);
	define_func("rpc", js_rpc, 3);
	define_func("ipc", js_ipc, 2);
	define_func("urlget", js_urlget, 1);
	define_func("beep", js_beep, 1);
	define_func("input", js_input, 1);
	define_func("termsize", js_termsize, 0);
	define_func("bind", js_bind, 3);
	define_func("unbind", js_unbind, 2);
	define_store_object();
	if (is_local) // Don't allow bbs user to use dbquery
		define_func("dbquery", js_dbquery, 2);
	duk_pop(ctx);
	return 0;
}
