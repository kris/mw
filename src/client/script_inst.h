#ifndef SCRIPT_INST_H
#define SCRIPT_INST_H

#include "script.h"

/*** Instruction set for the scripting language */

void scr_base( struct code *, int, char **);
void scr_beep( struct code *, int, char **);
void scr_boolopt( struct code *, int, char **);
void scr_case( struct code *, int, char **);
void scr_clearvars( struct code *, int, char **);
void scr_compare( struct code *, int, char **);
void scr_end( struct code *, int, char **);
void scr_exec( struct code *, int, char **);
void scr_getvar( struct code *, int, char **);
void scr_inlist( struct code *, int, char **);
void scr_input( struct code *, int, char **);
void scr_isanum( struct code *, int, char **);
void scr_ison( struct code *, int, char **);
void scr_jump( struct code *, int, char **);
void scr_listvars( struct code *, int, char **);
void scr_local( struct code *, int, char **);
void scr_logoninfo( struct code *, int, char **);
void scr_makestr( struct code *, int, char **);
void scr_math( struct code *, int, char **);
void scr_math2( struct code *, int, char **);
void scr_output( struct code *, int, char **);
void scr_print( struct code *, int, char **);
void scr_quit( struct code *, int, char **);
void scr_rand( struct code *, int, char **);
void scr_range( struct code *, int, char **);
void scr_regex( struct code *, int, char **);
void scr_return( struct code *, int, char **);
void scr_room( struct code *, int, char **);
void scr_roomnum( struct code *, int, char **);
void scr_say( struct code *, int, char **);
void scr_sayto( struct code *, int, char **);
void scr_sendipc( struct code *, int, char **);
void scr_sendrpc( struct code *, int, char **);
void scr_set( struct code *, int, char **);
void scr_showrunaway( struct code *, int, char **);
void scr_sleep( struct code *, int, char **);
void scr_split( struct code *, int, char **);
void scr_strcat( struct code *, int, char **);
void scr_string( struct code *, int, char **);
void scr_talkpriv( struct code *, int, char **);
void scr_time( struct code *, int, char **);
void scr_unset( struct code *, int, char **);
void scr_user( struct code *, int, char **);
void scr_version( struct code *, int, char **);
void scr_wholist( struct code *, int, char **);

int isanumul(const char *a, unsigned long *result, int onlydecimal);
int isanum(const char *a, int *result, int onlydecimal);

extern Instruction inst_table[];

#endif /* SCRIPT_INST_H */

