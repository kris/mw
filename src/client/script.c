#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <readline/readline.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdbool.h>
#include "bb.h"
#include "talker_privs.h"
#include "strings.h"
#include "str_util.h"
#include "expand.h"
#include "talker.h"
#include "init.h"
#include "alias.h"
#include "script_inst.h"
#include "main.h"
#include "frl.h"
#include "js.h"
#include "user.h"
#include "userio.h"
#include "who.h"
#include "gaglist.h"
#include <util.h>

#define MAX_ARGC 128

extern struct user * const user;
extern unsigned long rights;
extern int busy;
extern int current_rights;

struct function *function_list=NULL;
extern const char *autoexec_arg;
var_list_t var_list = -1;
var_list_t *local_vars=NULL;
CompStack *comparison_stack=NULL;

/* very crude event driver, called on text recv */
char *event_body_text=NULL;

/* set runaway variable */
unsigned long run_away = RUNAWAY;

/* set flood limit */
int flood_limit = FLOODLIMIT;

/* how many script threads are currently running */
int script_running=0;

/* person causing the event to occur */
char *event_user=NULL;

/* do we want to output text after an event? */
volatile int script_output = 1;

/* logon type */
int talker_logontype = 0;

/* User's home directory, for script filename expansion */
static char	       *homedir = NULL;
static size_t		homelen = 0;

void script_init(void)
{
	hash_setup();
	VAR_NEWLIST(&var_list);
}

char ** makeargs(int argc, const char **argv, int offset)
{
	int num;
	char **new;
	int i;

	num=argc-offset;
	new=(char **)calloc(num+1, sizeof(char *));

	for (i=0; i<num; i++)
	{
		new[i]=strdup(argv[offset+i]);
	}
	new[num]=NULL;

	return (new);
}

static char *debug_info(const char *filename, int lineno)
{
	char buff[512];
	snprintf(buff, 511, "%s:%d", filename, lineno);
	return ( strdup(buff) );
}

static int endchar(const char *haystack, char needle)
{
	int len;
	len=strlen(haystack);
	if (haystack[len-1] == needle) return (1);
	else return(0);
}

/* autoexecs any new initialisation functions */
void RunInitFuncs(int talkinit)
{
	struct function *func=NULL;

    	/* execute 'init' functions */
	func=function_list;
	while(func!=NULL)
	{
		if (func->new)
		{
			if (talkinit)
			{
				if (func->flags & FUNCFLAG_AUTOINIT)
				{
					ExecEvent(func->name,autoexec_arg,"AutoExec",NULL,0);
					func->new = false;
				}
			}
			else
			{
				if (func->flags & FUNCFLAG_BOARDINIT)
				{
					ExecEvent(func->name,autoexec_arg,"AutoExec",NULL,0);
					func->new = false;
				}
			}
		}
		func=func->next;
	}
}

FuncNames scrtc[]={
{"talkname"	,part_who_talk},
{"whoname"	,part_who},
{"username"	,part_user},
{"gag"		,gaglist_tc_filter},
{"bind"		,list_bind},
{"boardexec"	,list_commands},
{"exec"		,list_chat_commands},
{"script"	,list_script},
{"null"		,NULL},
{NULL		,NULL}
};

void LoadFunction(char *funcname, char *header, FILE *file, int *lineno, const char *filename, int flags)
{
	struct function *new;
	char *buff = NULL;
	char *line = NULL;
	const char *bits[MAX_ARGC];
	int num;
	struct label *label=NULL;
	struct code *codeend=NULL, *newcode;
	Instruction *inst;
	/* new vars for tab-completion */
	const char *argv[MAX_ARGC];
	int argc;
	int lp;
	FuncNames *fn = NULL;
	int found;

	new=function_list;
	while (new!=NULL && strcasecmp(funcname, new->name)) new=new->next;

	if (new!=NULL)
	{
		printf("Function %s already exists. Redefined at line %d in %s.\n", funcname, *lineno, filename);
		buff = NULL;
		while (!feof(file))
		{
			if ((buff = frl_line_nspace(file, "#")) == NULL) break;
			*lineno+=num_lines_read();
			if (!strcasecmp(buff, "endfunc")) break;
			free(buff);
			buff = NULL;
		}
		if (buff) free(buff);
		return;
	}

	/* insert new function into the list */
	new=(struct function *)malloc(sizeof(struct function));
	new->name=strdup(funcname);
	new->new=true;
	new->code=NULL;
	new->jump=NULL;
	new->flags = flags;

	/* add current function to function list */
	/* new ordered insert - so tab-completion for script names works */
	{
		struct function *prev, *cur;
		cur = function_list;
		prev = NULL;
		while (cur!=NULL && strcmp(cur->name,new->name) < 0)
		{
			prev=cur;
			cur=cur->next;
		}
		/* start of list */
		if (prev == NULL)
		{
			new->next = function_list;
			function_list = new;
		}
		/* end of list */
		else if (cur == NULL)
		{
			prev->next = new;
			new->next = NULL;
		}
		/* middle of list */
		else
		{
			new->next = cur;
			prev->next = new;
		}
	}

	/* work out tab-completion status information */
	new->numcomp = 0;
	new->complist = NULL;
	argc = ParseLine(header, argv);
	if (argc <= 2)
	{
		new->numcomp = 1;
		new->complist = malloc(sizeof(CompletionList));
		new->complist[0].Command = "";
		new->complist[0].Mode = 2;
		new->complist[0].FArg = 1;
		new->complist[0].LArg = -1;
		new->complist[0].CPFunction = part_who;
	}
	else
	{
		for (lp=2; lp<argc; lp++)
		{
			/* go through tab complete list */
			fn = scrtc;
			found = 0;
			while(fn->name)
			{
				if (!strcasecmp(fn->name, argv[lp]))
				{
					found = 1;
					break;
				}
				fn++;
			}
			/* handle special 'ad infinitum' sequence */
			if (!strcasecmp("...", argv[lp])) found = 2;
			/* if not found, skip arguments */
			if (!found)
			{
				printf("Invalid argument type '%s' for function %s, on line %d in %s\n", argv[lp], funcname, *lineno, filename);
				break;
			}
			else
			{
				/* completion function */
				new->numcomp++;
				new->complist = realloc(new->complist, sizeof(CompletionList) * new->numcomp);
				new->complist[new->numcomp - 1].Command = "";
				new->complist[new->numcomp - 1].Mode = 2;
				new->complist[new->numcomp - 1].FArg = lp-1;
				new->complist[new->numcomp - 1].LArg = (found==2)?-1:lp-1;
				new->complist[new->numcomp - 1].CPFunction = fn->function;
				/* if ad infinitum, skip rest of arguments */
				if (found == 2) break;
			}
		}
	}

	while (!feof(file) && (line = frl_line(file)))
	{
		(*lineno)++;

		if (strchr("#\n\r", line[0])!=NULL) { free(line); continue; }

		num=ParseLine(line, bits);
		if (num==0) { free(line); continue; }
		if (bits[0][0]=='#') { free(line); continue; }
		if (!strcasecmp(bits[0], "endfunc")) break;

		if (endchar(bits[0], ':'))
		{
			char *p;
			if ((p=strrchr(bits[0],':'))!=NULL) *p=0;
			label=new->jump;
			while (label!=NULL && strcasecmp(bits[0], label->name))
				label=label->next;
			if (label!=NULL)
			{
				printf("Label %s referenced at %s:%d already in use at %s\n", label->name, filename, *lineno, label->debug);
				label=NULL;
				free(line);
				continue;
			}
			label=(struct label *)malloc(sizeof(struct label));
			label->name=strdup(bits[0]);
			label->debug=debug_info(filename, *lineno);
			label->next=new->jump;
			label->where=NULL; /* fill in later */
			new->jump=label;
		}else
		{
			inst=inst_table;
			while (inst->name != NULL && strcasecmp(inst->name, bits[0])) {
				inst++;
			}
			if (inst->name==NULL)
			{
				printf("Invalid instruction '%s' at line %d of file %s\n", bits[0], *lineno, filename);
				free(line);
				continue;
			}
			newcode=(struct code *)malloc(sizeof(struct code));
			newcode->inst=inst;
			newcode->argc=num-1;
			newcode->argv=makeargs(num, bits, 1);
			newcode->debug=debug_info(filename, *lineno);
			newcode->parent=new;
			newcode->next=NULL;

			if (new->code==NULL)
			{
				new->code=newcode;
				codeend=newcode;
			}else
			{
				codeend->next=newcode;
				codeend=newcode;
			}

			/* fill in the labels */
			if (label!=NULL)
			{
				label=new->jump;
				while (label!=NULL) {
					if (label->where==NULL)
						label->where=newcode;
					label=label->next;
				}
				label=NULL;
			}
		}
		free(line);
	}
	if (line) free(line);

	/* automatic return at the end of a function if theres a label hanging open. */
	if (new->code!=NULL && label!=NULL) {
		inst=inst_table;
		while (inst->name != NULL && strcasecmp(inst->name, "return")) {
			inst++;
		}
		newcode=(struct code *)malloc(sizeof(struct code));
		newcode->inst=inst;
		newcode->argc=0;
		newcode->argv=NULL;
		newcode->debug=debug_info(filename, *lineno);
		newcode->parent=new;
		newcode->next=NULL;
		codeend->next=newcode;

		label=new->jump;
		while (label!=NULL) {
			if (label->where==NULL)
				label->where=newcode;
			label=label->next;
		}
		label=NULL;
	}
}

void DestroyFunction(const char *funcname)
{
	struct function *func, *funcold;
	struct code *fcode;
	struct label *label;

	func=function_list;
	funcold=NULL;
	while (func!=NULL && strcasecmp(funcname, func->name))
	{
		funcold=func;
		func=func->next;
	}
	if (func==NULL) {
		printf("Cant find function %s to destroy.\n", funcname);
		return;
	}
	if (funcold==NULL)
		function_list=func->next;
	else
		funcold->next=func->next;
	free(func->name);

	label=func->jump;
	while (label!=NULL) {
		struct label *oldlabel;
		oldlabel=label->next;
		free (label->name);
		free (label->debug);
		free (label);
		label=oldlabel;
	}

	fcode=func->code;
	while (fcode!=NULL) {
		int i;
		struct code *oldcode;
		free(fcode->debug);
		for (i=0;i<fcode->argc;i++)
			free(fcode->argv[i]);
		free(fcode->argv);
		oldcode=fcode->next;
		free(fcode);
		fcode=oldcode;
	}

	free(func->complist);

	free(func);
	printf("Function %s destroyed.\n",funcname);
}

void DestroyAllFunctions(int debug)
{
	struct function *func;
	struct code *fcode;
	struct label *label;

	while (function_list!=NULL)
	{
		if (debug) printf("Destroying Function %s.\n",function_list->name);

		free(function_list->name);
		label=function_list->jump;
		while (label!=NULL)
		{
			struct label *oldlabel;
			oldlabel=label->next;
			free (label->name);
			free (label->debug);
			free (label);
			label=oldlabel;
		}
		fcode=function_list->code;
		while (fcode!=NULL)
		{
			int i;
			struct code *oldcode;
			free(fcode->debug);
			for (i=0;i<fcode->argc;i++)
				free(fcode->argv[i]);
			free(fcode->argv);
			oldcode=fcode->next;
			free(fcode);
			fcode=oldcode;
		}
		free(function_list->complist);
		func = function_list;
		function_list=function_list->next;
		free(func);
	}
}


/***************
 *** HELP
 ***************/

void scr_helpfile(const char *args)
{
	Instruction *inst;

	if (args!=NULL &&(strchr(args,'.')!=NULL || strchr(args,'/')!=NULL))
	{
		printf("Sorry, no help available on that subject. *:-)\n");
		return;
	}

	if (args==NULL)
	{
		const char *x = SCRIPTHELP"/general";

		if (!access(x,00))
			printfile(x);
		else
			printf("No general help available on scripts.\n");
	}else
	{
		_autofree char *x = NULL;

		inst=inst_table;
		while (inst->name != NULL && strcasecmp(inst->name, args)) {
			inst++;
		}
		if (inst->name!=NULL)
		{
			if ((rights&inst->Rights) != inst->Rights)
			{
				printf("Sorry, no help available on that subject. *:-)\n");
				return;
			}
		}
		asprintf(&x, SCRIPTHELP"/%s", args);
		if (x != NULL && !access(x,00))
			printfile(x);
		else
			printf("No help available on that subject.\n");
	}
}

void scr_helplist(void)
{
	int count;
	char buff[10];
	Instruction *c = inst_table;
	int screen_height = screen_h();

	count=0;
	printf("Available commands:-\n");
	while (c->name)
	{
		if ((rights&c->Rights) == c->Rights) {
			printf("%20s - %s\n",c->name,c->help);
			count++;
		}
		c++;
		if (count>=(screen_height-2))
		{
			printf("---more---\r");
			get_str(buff, 5);
			printf("          \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}
	}
}

void ListVars(const char *srch)
{
	var_op_t op;
	int count=0;
	int screen_height = screen_h();
	char buff[MAXTEXTLENGTH];

	if (local_vars)
	    VAR_LIST_ITERATE(&op, local_vars);
	else
	    VAR_LIST_ITERATE(&op, &var_list);
	while (VAR_FOUND(&op))
	{
		if (srch == NULL || !strncasecmp(srch, VAR_STR_KEY(&op), strlen(srch)))
		{
			snprintf(buff, MAXTEXTLENGTH-1, "Variable '%s'", VAR_STR_KEY(&op));
			if (var_str_val(&op)==NULL)
				strncat(buff, " is set but invalid\n", MAXTEXTLENGTH - strlen(buff) - 1);
			else
			{
				strncat(buff, " contains value '", MAXTEXTLENGTH - strlen(buff) - 1);
				strncat(buff, var_str_val(&op), MAXTEXTLENGTH - strlen(buff) - 1);
				strncat(buff, "'\n", MAXTEXTLENGTH - strlen(buff) - 1);
			}
			/*display_message(buff, 0, 1);*/
			escprintf("%s", buff);
			count++;
		}

		if (count>=(screen_height-2))
		{
			printf("---more---\r");
			get_str(buff, 5);
			printf("          \r");
			if (*buff=='q' || *buff=='Q') return;
			count=0;
		}
		VAR_LIST_NEXT(&op);
	}
}

/***************
 ** Runtime...
 ***************/

/* conditionals */
int compare_count;
int compare_match;
int script_offset=0;  /* how far have we 'shift'ed the args */
struct code *script_jump;

unsigned long runaway=0;  /* count how many instructions executed to stop infinite loops */
int flood=0; /* stop room flooding by a script */
int script_debug=0;  /* print verbose debug info */
int script_terminate;

int ExecInst(char *line)
{
	const char *bits[MAX_ARGC];
	int num;
	Instruction *inst;
	struct code *pc;

	num=ParseLine(line, bits);
	if (num==0)
	{
		printf("- WARNING (ExecInst): No Instruction to Execute\n");
		return 0;
	}

	inst=inst_table;
	while (inst->name != NULL && strcasecmp(inst->name, bits[0])) {
		inst++;
	}
	if (inst->name==NULL)
	{
		printf("- WARNING (ExecInst): Invalid Instruction (%s)\n", bits[0]);
		return 0;
	}

	pc=(struct code *)malloc(sizeof(struct code));
	pc->inst=inst;
	pc->argc=num-1;
	pc->argv=makeargs(num, bits, 1);
	pc->debug=strdup("ExecInst");
	pc->parent=NULL;
	pc->next=NULL;

	if ((rights&pc->inst->Rights) == pc->inst->Rights)
	{
		pc->inst->Function(pc, 0, 0);
	}

	free(pc->debug);
	for (num=0; num < pc->argc; num++)
	{
		free(pc->argv[num]);
	}
	free(pc->argv);
	free(pc);

	return 1;
}

void DoScript(char *line)
{
	const char *bits[MAX_ARGC];
	var_list_t args;
	int num;
	int i;
	char *function_name;
	flood=0;

	if ((num=ParseLine(line, bits))<1) return;

	if ((function_name = FindLinks(bind_list, bits[0])) == NULL) {
		printf("Script bind '%s' not found.\n", bits[0]);
		return;
	}

	if (is_js(function_name)) {
		js_exec(function_name, num, bits);
		free(function_name);
		return;
	}
	free(function_name);
	runaway=0;
	script_terminate=0;
	script_jump=NULL;
	compare_count=0;
	compare_match=0;
	script_offset=0;
	script_running=1;
	if (event_user!=NULL) free(event_user);
	event_user=NULL;
	busy++;

	VAR_NEWLIST(&args);
	ARG_RANGE(&args, 0, num-1);
	for (i=0; i<num; ++i)
	    ARG_STR_FORCE(&args, i, bits[i]);

	switch (ExecScript(bits[0], &args, 1))
	{
	case 1:
		printf("Script function for bind '%s' not found.\n", bits[0]);
		break;
	case 2:
		printf("Script bind '%s' not found.\n", bits[0]);
		break;
	default:
		break;
	}

	VAR_FREELIST(&args);
	script_running=0;
	busy--;
}

static void Push_Comparisons(int cc, int cm)
{
	CompStack *temp;

	temp=malloc(sizeof(CompStack));
	temp->compare = cc;
	temp->match = cm;
	temp->next = comparison_stack;
	comparison_stack = temp;
}

static void Pop_Comparisons(int *cc, int *cm)
{
	CompStack *temp;

	if ((temp = comparison_stack) == NULL)
	{
		*cm = 0;
		*cc = 0;
	}
	else
	{
		*cc = temp->compare;
		*cm = temp->match;
		comparison_stack = comparison_stack->next;
		free(temp);
	}
}

int ExecScript(const char *name, var_list_t *vars, int bound)
{
	struct function *script;
	struct code *pc;
	char *function_name=NULL;
	int old_rights;

	/* publically named scripts */
	if (bound)
	{
		if ((function_name = FindLinks(bind_list, name)) == NULL)
		{
			return(2);
		}
	}
	/* otherwise use the name were given */
	if (function_name==NULL) function_name=strdup(name);

	/* look for a defined function by that name */
	script=function_list;
	while (script!=NULL && strcasecmp(function_name, script->name)) script=script->next;
	if (script==NULL)
	{
		/* okay, so its not a mwscript function */
		free(function_name);
		return(1);
	}

	/* backup old rights, and set talker rights */
	old_rights = current_rights;
	set_talk_rights();

	/* push current comparison counters onto the stack. */
	/* first time this happens, values are ignored, as we use current values, not those on stack. */
	/* this is horrible, but cant be bothered to change _everything_ */
	Push_Comparisons(compare_count, compare_match);
	compare_count = 0;
	compare_match = 0;

	/*found script and now runing */
	pc=script->code;
	local_vars = vars;
	while (pc!=NULL && !script_terminate)
	{
		runaway++;
		if ((run_away > 0) && (runaway > run_away))
		{
			printf("Runaway script got to %lu instructions at %s\n", runaway, pc->debug);
			break;
		}

		if (script_debug)
		{
			int	i;

			escprintf("%s: %s ", pc->debug, pc->inst->name);
			for (i=0;i<pc->argc;i++) escprintf("'%s' ",pc->argv[i]);
			printf("\n");
		}

		if ((rights&pc->inst->Rights) == pc->inst->Rights)
		{
			pc->inst->Function( pc, 0, 0);
			local_vars = vars;
		}

		pc=pc->next;
		if (script_jump!=NULL)
		{	/* execute a goto */
			pc=script_jump;
			script_jump=NULL;
		}
	}

	/* pop old comparison counters off the stack */
	Pop_Comparisons(&compare_count, &compare_match);

	if (script_terminate==1) script_terminate=0;
	free(function_name);
	local_vars = NULL;

	/* restore old rights */
	if (old_rights == RIGHTS_BBS) set_rights();

	return(0);
}

int ExecEvent(char *script, const char *text, const char *event, char *who, int pre)
{
	int retval;
	var_list_t args;

	if (is_js(script)) {
		const char *argv[4];
		argv[0]=event;
		argv[1]=who;
		argv[2]=text;
		argv[3]=text+pre;
		busy++;
		retval=js_exec(script, 4, argv);
		busy--;
		return retval;
	}
	runaway=0;
	if (!script_running) { flood=0;}
	script_terminate=0;
	script_jump=NULL;
	compare_count=0;
	compare_match=0;
	script_offset=0;
	if (event_user!=NULL) free(event_user);

	VAR_NEWLIST(&args);
	ARG_RANGE(&args, 0, 3);
	ARG_STR_FORCE(&args, 0, event);
	ARG_STR_FORCE(&args, 1, text);
	if (who!=NULL)
	{
	    event_user=strdup(who);
	    /*ARG_STR_FORCE(&args, 2, strdup(who)); */
	}else
	{
	    event_user=NULL;
	    /*ARG_STR_FORCE(&args, 2, strdup("")); */
	}
	if (event_body_text!=NULL) free(event_body_text);
	event_body_text = strdup(text+pre);
	/*ARG_STR_FORCE(&args, 3, strdup(event_body_text)); */

	busy++;
	retval = ExecScript(script, &args, 0);
	VAR_FREELIST(&args);
	busy--;

	return(retval);
}


int ExecEvent2(char *script, const char *event, char *who, int pre, int numargs, char *aargs[])
{
	var_list_t	args;
	int		retval, i;

	if (is_js(script)) {
		const char **argv;
		argv = calloc(numargs+2,sizeof(char *));
		argv[0]=event;
		argv[1]=who;
		for (i=0;i<numargs;i++)
			argv[i+2]=aargs[i];
		retval = js_exec(script, numargs+2, argv);
		free(argv);
		return retval;
	}
	runaway=0;
	if (!script_running) { flood=0;}
	script_terminate=0;
	script_jump=NULL;
	compare_count=0;
	compare_match=0;
	script_offset=0;
	script_output=1;
	if (event_user!=NULL) free(event_user);

	VAR_NEWLIST(&args);
	ARG_RANGE(&args, 0, numargs+1);
	ARG_STR_FORCE(&args, 0, event);

	for (i = 0; i < numargs; i++)
	{
		ARG_STR_FORCE(&args, i + 1, aargs[i]);
	}

	if (who!=NULL)
	{
	    event_user=strdup(who);
	}else
	{
	    event_user=NULL;
	}

	if (event_body_text!=NULL) free(event_body_text);

	if (numargs == 0)
		event_body_text = strdup("");
	else
		event_body_text = strdup(aargs[0] + pre);

	busy++;
	retval = ExecScript(script, &args, 0);
	VAR_FREELIST(&args);
	busy--;

	return(retval);
}

void ListScript(const char *name)
{
	struct function *ptr;
	int i;

	ptr=function_list;

	if (name==NULL)
		ShowLinks(bind_list, "Available Script Binds :-", "-->", 0);
	else
	{
		while (ptr!=NULL && strcasecmp(name, ptr->name)) ptr=ptr->next;
		if (ptr==NULL)
			printf("Function %s not found.\n", name);
		else
		{
			for (struct code *c = ptr->code; c != NULL; c = c->next)
			{
				printf("%s: %s %d ", c->debug, c->inst->name, c->argc);
				for (i=0; i<c->argc; i++) printf("'%s' ", c->argv[i]);
				printf("\n");
			}
		}
	}
}

char *eval_arg(char *arg, int argc, char **argv)
{
    const char *value = eval_str(arg);
    if (value)
	return strdup(value);
    else
    {
	printf("Parse error in expansion of \"%s\": %s.\n", arg, parse_error);
	return strdup("{error}");
    }
}

char ** argv_shift(int argc, char **argv, int offset, int *newsize)
{
	char **new;
	char buff[13];
	int i, idx, num;
	char *text;
	int fargc;
	char **fargv = 0;

	num=argc-offset;

	fargc = arg_count(local_vars);
	*newsize=0;
	for (i=0; i<num; i++)
	{
		text = eval_arg(argv[offset+i], fargc, fargv);
		if (!strncasecmp(text, "$^", 2))
		{
			int start=2;
			if (strlen(text)>2)
			{
				int ts;
				ts = atoi(&text[2]);
				if (ts > start) start = ts;
			}
			(*newsize)+=fargc-start;
		}
		else (*newsize)++;
		free(text);
	}

	/* fixed problem if there arent enough arguments to cope with $^n */
	if (*newsize < 0) *newsize = 0;

	new=(char **)calloc((*newsize)+1, sizeof(char *));

	idx=0;
	for (i=0; i<num; i++)
	{
		text = eval_arg(argv[offset+i], fargc, fargv);
		if (!strncasecmp(text, "$^", 2))
		{
			int j;
			int start=2;

			if (strlen(text)>2)
			{
				int ts;
				ts = atoi(&text[2]);
				if (ts > start) start = ts;
			}

			for (j=0; j<fargc-start; j++)
			{
				snprintf(buff, 12, "$%d", j+start);
				new[idx]=eval_arg(buff, fargc, fargv);
				idx++;
			}
		}
		else
		{
			new[idx]=eval_arg(argv[offset+i], fargc, fargv);
			idx++;
		}
		free(text);
	}

	new[idx]=NULL;
	return (new);
}

void DestroyVariables(int debug)
{
	VAR_FREELIST(&var_list);
}

void ScriptCleanup(void)
{
	DestroyAllFunctions(0);
	DestroyVariables(0);

	expand_close();
	hash_shutdown();
	if (homedir) free (homedir);
}

void scr_devel_msg(struct code *pc, const char *fmt, ...)
{
	va_list va;
	char text[MAXTEXTLENGTH];

	va_start(va, fmt);
	vsnprintf(text, MAXTEXTLENGTH-1, fmt, va);
	va_end(va);

	if (cp_test(user, CP_DEVEL))
		printf("- WARNING (%s): %s, in %s\n", pc->inst->name, text, pc->debug);
}

/* script function autocompletion for readline */
char *list_script(const char *text, int state)
{
	static int len;
	static struct function *fn = NULL;
	char *name;
	int hidden;

	if (state == 0)
	{
		fn = function_list;
		len = strlen(text);
	}

	while (fn != NULL)
	{
		hidden = fn->flags & FUNCFLAG_LOCAL;
		name = fn->name;
		fn = fn->next;
		if (len == 0 || !strncasecmp(name, text, len))
		{
			/* only if an interface function (default) */
			if (hidden == false)
			{
				return dupstr(name, "");
			}
		}
	}
	return NULL;
}

/* script file autocompletion for readline */
char *list_mwsfile(const char *text, int state)
{
    char	       *relname;
    char	       *name;
    char	       *trimmed_name;
    struct stat		stats;
    int			isdir;
    struct passwd      *pw;
    char	       *part_path;

    if (!state && !homedir)
    {
	/* Can we find the user's home directory? */
	if ((pw=getpwuid(getuid()))!=NULL)
	{
	    /* Copy the home directory */
	    homedir = strdup(pw->pw_dir);
	    homelen = strlen(homedir) + 1;
	}
    }

    relname = NULL;
    /* Is the path prefix legal ? */
    if (text &&
	strncmp(text, "../", 3) != 0 &&
	strstr(text, "/../") == NULL)
    {
	/* Set euid to real uid so completion can't be used to read Arthur's
	 * home directory */
	perms_drop();
	/* Keep getting filenames until we see a legal one */
	do
	{
	    /* Use readline's file completion to do the dirty work */
	    name = rl_filename_completion_function(text, state);
	    state = 1;
	    if (name)
	    {
		/* Is the file path legal? */
		if (strcmp(name, ".") != 0 &&
		    strcmp(name, "./") != 0 &&
		    strncmp(name, "../", 3) != 0 &&
		    strstr(name, "/../") == NULL)
		{
		    part_path = strdup(name);
		    expand_script_dir(&part_path);
		    if (stat(part_path, &stats) == 0)
		    {
			/* See if this file is a directory */
			isdir = S_ISDIR(stats.st_mode);
			if (isdir || S_ISREG(stats.st_mode))
			{
			    trimmed_name = name;
			    while (strncmp(trimmed_name, "./", 2) == 0)
				trimmed_name += 2;
			    /* Copy the relative part (after our home dir) */
			    relname = strdup(trimmed_name);
			}
		    }
		    free(part_path);
		}
		/* Free the filename, bypassing the malloc wrappers */
		free(name);
	    } else
	    {
		break;
	    }
	} while (!relname);
	perms_restore();
    }

    return relname;
}

int expand_script_dir(char **dir)
{
    char	       *old;
    char	       *part_path;

    if (homedir && dir && *dir)
    {
	old = *dir;
	/* Combine the home dir & partial filename */
	if (strcmp(old, ".") == 0)
	{
	    part_path = strdup(homedir);
	}
	else
	{
	    while (strncmp(old, "./", 2) == 0)
		old += 2;
	    part_path = malloc(homelen + strlen(old) + 1);
	    sprintf(part_path, "%s/%s", homedir, old);
	}
	/* Replace the relative path with an absolute path */
	free(*dir);
	*dir = part_path;
    }

    /* Return of 0 indicates that this modification should not be shown in the
     * input line
     */
    return 0;
}
