/**********************************************************************************
  NOTE: The global colour set is freed everytime if reloaded, but it doesnt need
        to get freed normally, so reports as a memory leak. To fix this, have created
        simple destroy routine called at end of close_down, although the OS should
        free all the memory anyway.
**********************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include "frl.h"
#include "colour.h"
#include "user.h"
#define COLOUR_LIMIT 40

static  char *colour_chart[COLOUR_LIMIT];
static char *colour_set=NULL;

/* the current user template */
extern struct user * const user;

/* return colour code sequence */
char *colour(char *text)
{
	static char line[40];
	int i;

	line[0]=0;

	/* system colour chart */
	if (strchr("0123456789",text[0]))
	{
		i=atoi(text);

		if (i<0 || i>= COLOUR_LIMIT) return(line);
		if (colour_chart[i]==NULL)
			if (colour_chart[0]==NULL)
				return(line);
			else
				return (colour_chart[0]);
		else
			return (colour_chart[i]);
	}

	i=0;
	line[i++]=033;
	line[i++]='[';

	/* reset string ? */
	if (strchr("-n", text[0]) &&
	    strchr("-n", text[1]) )
	{
		line[i++]='m';
		line[i]=0;
		return(line);
	}

	/* high intensity mode? */
	if (isupper(text[0]))
	{
		line[i++]='1';
		line[i++]=';';
	} else {
		line[i++]='0';
		line[i++]=';';
	}

	if (strchr("kKrRgGyYbBmMcCwW", text[0])!=NULL)
	{
		if (strchr("n-", text[0])==NULL) line[i++]='3';
		if (strchr("kK",text[0])) line[i++]='0'; else
		if (strchr("rR",text[0])) line[i++]='1'; else
		if (strchr("gG",text[0])) line[i++]='2'; else
		if (strchr("yY",text[0])) line[i++]='3'; else
		if (strchr("bB",text[0])) line[i++]='4'; else
		if (strchr("mM",text[0])) line[i++]='5'; else
		if (strchr("cC",text[0])) line[i++]='6'; else
		if (strchr("wW",text[0])) line[i++]='7';

		/* seperator */
		if (strchr("n-",text[0])==NULL && strchr("n-",text[1])==NULL) line[i++]=';';
	}

	if (strchr("kKrRgGyYbBmMcCwW", text[1])!=NULL)
	{
		/* now background colour */
		if (strchr("n-",text[1])==NULL) line[i++]='4';
		if (strchr("kK",text[1])) line[i++]='0'; else
		if (strchr("rR",text[1])) line[i++]='1'; else
		if (strchr("gG",text[1])) line[i++]='2'; else
		if (strchr("yY",text[1])) line[i++]='3'; else
		if (strchr("bB",text[1])) line[i++]='4'; else
		if (strchr("mM",text[1])) line[i++]='5'; else
		if (strchr("cC",text[1])) line[i++]='6'; else
		if (strchr("wW",text[1])) line[i++]='7';
	}

	line[i++]='m';
	line[i]=0;
	return(line);
}

void colour_free(void)
{
	int i;

	if (colour_set!=NULL) free(colour_set);
	for (i=0;i<COLOUR_LIMIT;i++)
		if (colour_chart[i]!=NULL) free(colour_chart[i]);
}

void colour_load(char *file, int quiet)
{
	FILE *ff;
	char *buff;
	int i;
	int count=0;

	if ((ff=fopen(file,"r"))==NULL)
	{
		printf("Error: Cannot open %s:%s\n",file, strerror(errno));
		return;
	}

	for (i=0;i<COLOUR_LIMIT;i++)
	{
		if (colour_chart[i]!=NULL) free(colour_chart[i]);
		colour_chart[i]=NULL;
	}
	/* throw away first line */
	if (colour_set!=NULL) free(colour_set);
	colour_set = frl_line_nspace(ff, "#");
	if (!quiet) printf("Loading colour scheme: %s\n", colour_set);

	while (!feof(ff) &&  ((buff = frl_line_nspace(ff, "#"))!=NULL))
	{
		char *c;

		if ((c=strtok(buff," \t"))==NULL) { free(buff); continue; }
		i=atoi(c);
		if (i<0 || i>=COLOUR_LIMIT) { free(buff); continue; }

		if ((c=strtok(NULL, "\n"))==NULL) { free(buff); continue; }

		while (*c==' ' || *c=='\t') c++;

		if (colour_chart[i]!=NULL)
			if (!quiet) printf("Warning: redefining colour %d\n",i);

		colour_chart[i]=strdup(c);
		count++;
		free(buff);
	}
	if (!quiet) printf("Loaded %d colour definitions.\n",count);
}

void init_colour(void)
{
	int i;
	char path[1024];

	for (i=0;i<COLOUR_LIMIT;i++) colour_chart[i]=NULL;

	snprintf(path, 1023, COLOURDIR"/%d.col", user->record.colour);
	if (access(path, R_OK))
	{
		printf("Error loading colour scheme %d.\n", user->record.colour);
		printf("Changing to default scheme.\n");
		user->record.colour=0;
		snprintf(path, 1023, COLOURDIR"/%d.col", user->record.colour);
	}
	colour_load(path, 1);

	/* CFRY: need to find default terminal fore/back colour, and whether it supports the 'concealed' ascii-seqence for text */

}

char *get_colour(void)
{
	return(colour_set);
}

void destroy_colours(void)
{
	int i;

	for (i=0;i<COLOUR_LIMIT;i++)
	{
		if (colour_chart[i]!=NULL) free(colour_chart[i]);
		colour_chart[i]=NULL;
	}
	if (colour_set!=NULL) free(colour_set);
	colour_set=NULL;
}
