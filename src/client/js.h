#ifndef JS_H
#define JS_H

#include <stdio.h>

int js_isrunning(void);
int js_exec(char *name, int argc, const char **argvc);
int load_jsfile(FILE *f, const char *filename);
int is_js(char *name);
void js_stop_execution(void);
int stop_js(void);
int setup_js(void);

enum bindtype {
	K_BIND = 0,
	K_BIND_EVENT,
	K_BIND_ONOFF,
	K_BIND_IPC,
	K_BIND_FORCE,
	K_BIND_SHUTDOWN,
	K_BIND_ALIAS,
	K_BIND_RPC,
	K_BIND_INPUT
};
#define K_BROADCAST 1

#endif /* JS_H */
