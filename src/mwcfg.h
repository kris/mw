#ifndef MWCFG_H
#define MWCFG_H

struct cfg_default_opt {
#define CFG_OPT_NULL (0)
#define CFG_OPT_INT  (1)
#define CFG_OPT_BOOL (2)
#define CFG_OPT_STR  (3)
	int o_type;
	const char *o_name;
	union {
#define _OPTTYP_INT integer
		long integer;
#define _OPTTYP_BOOL boolean
		int boolean;
#define _OPTTYP_STR string
		const char *string;
	} o_val;
};

#define _OPTTYP(t) _OPTTYP_##t

#define CFG_OPT(type, name, value) { \
	.o_type = CFG_OPT_##type, \
	.o_name = name, \
	.o_val._OPTTYP(type) = value }
#define CFG_END { 0, NULL, {0} }

extern int cfg_init_defaults(const struct cfg_default_opt *dft_opts);
extern int cfg_load(const char *cfg_file);
extern void cfg_dump(FILE *f);

extern int cfg_set_int(const char *keyname, long int_val);
extern int cfg_set_bool(const char *keyname, int bool_val);
extern int cfg_set_string(const char *keyname, const char *str_val);

extern int cfg_get_bool(const char *keyname);
extern long cfg_get_int(const char *keyname);
extern const char *cfg_get_string(const char *keyname);
#endif /* MWCFG_H */
