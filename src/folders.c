#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "strings.h"
#include "folders.h"
#include "perms.h"
#include "files.h"

#define FOLDERFILE  STATEDIR"/folders.bb"

int idx_fd = -1;
int txt_fd = -1;

const char *folder_types[] = {
	FOL_LIVE,
	FOL_MOD,
	FOL_NEW,
	FOL_TMP,
};

int openfolderfile(int mode)
{
	int x;
	x=open(FOLDERFILE,mode);
	if (x<0)
		perror(FOLDERFILE);
	return(x);
}

int nofolders(void)
{
	int fd;
	struct folder fol;

	fd = open(FOLDERFILE, O_RDONLY);
	if (fd < 0) return 1;

	if (read(fd,&fol,sizeof(struct folder)) < sizeof(struct folder))
	{
		close(fd);
		return 1;
	}

	close(fd);
	return 0;
}

int create_folder_file(void)
{
	int file;
	int ret;

	file = open(FOLDERFILE, O_WRONLY|O_CREAT, 0600);
	if (file < 0) {
		perror(FOLDERFILE);
		return 1;
	}
	Lock_File(file);
	ret = ftruncate(file, sizeof(struct folder) * 64);
	if (ret != 0)
		perror("Truncating folder file");
	Unlock_File(file);
	close(file);
	return ret;
}

int foldernumber(const char *name)
{
	/* return number of folder name */
	int file;
	int number=0;
	int no;
	struct folder fold;
	size_t len = strlen(name);

	if (len == 0) return -1;
	if ((file=openfolderfile(O_RDONLY)) < 0) return -1;
	do{
		no=get_folder_entry(file,&fold);
		number++;
	}while (no!=0 && strncasecmp(name,fold.name,len));
	close(file);

	if (no==0) return(-1); else return(number-1);
}

int get_folder_entry(int file, struct folder *tmp)
{
	int no;
	if ((no=read(file,tmp,sizeof(*tmp)))<0)
	{
		perror("get_folder_entry");
		exit(-1);
	}
	return(no);
}

int get_folder_number(struct folder *fol, int num)
{
	int file;

	if (nofolders())
		return 0;
	if ((file=openfolderfile(O_RDONLY)) < 0) return 0;
	if (pread(file, fol, sizeof(*fol), sizeof(*fol) * num) < 0)
	{
		perror("get_folder_number");
		return 0;
	}
	close(file);
	return 1;
}

int folders_init(void)
{
	/* Grab fds for the top bbs directories so we can use openat(), renameat(),
	   etc. later instead of building paths. */
	idx_fd = open(FOL_IDX_DIR, O_RDONLY);
	if (idx_fd < 0) {
		perror(FOL_IDX_DIR);
		return 1;
	}
	txt_fd = open(FOL_TXT_DIR, O_RDONLY);
	if (txt_fd < 0) {
		perror(FOL_TXT_DIR);
		return 1;
	}
	return 0;
}

static int open_folder_path(int top_fd, struct folder *folder, const char *subdir, int flags, int mode)
{
	int fd;
	int at_fd = top_fd;

	if (subdir != NULL && *subdir != '\0') {
		at_fd = openat(top_fd, subdir, O_RDONLY, 0);
		if (at_fd < 0) {
			perror(subdir);
			return -1;
		}
	}
	fd = openat(at_fd, folder->name, flags, mode);
	if (at_fd != top_fd)
		close(at_fd);
	if (fd < 0)
		perror(folder->name);
	return fd;
}

int open_folder_index(struct folder *folder, const char *fol_type, int flags, int mode)
{
	return open_folder_path(idx_fd, folder, fol_type, flags, mode);
}

int open_folder_text(struct folder *folder, const char *fol_type, int flags, int mode)
{
	return open_folder_path(txt_fd, folder, fol_type, flags, mode);
}

int rename_folder(struct folder *folder, const char *newname)
{
	unsigned i;
	int fds[] = {idx_fd, txt_fd, -1, -1, -1};

	if (f_moderated(folder->status) || f_moderated(folder->g_status)) {
		fds[2] = openat(idx_fd, FOL_MOD, O_RDONLY);
		if (fds[2] < 0) {
			perror(FOL_IDX_DIR"/"FOL_MOD);
			return 1;
		}
		fds[3] = openat(txt_fd, FOL_MOD, O_RDONLY);
		if (fds[3] < 0) {
			perror(FOL_TXT_DIR"/"FOL_MOD);
			close(fds[2]);
			return 1;
		}
	}
	for (i = 0; fds[i] != -1; i++)
		renameat(fds[i], folder->name, fds[i], newname);

	close(fds[2]);
	close(fds[3]);
	strncpy(folder->name, newname, FOLNAMESIZE);
	folder->name[FOLNAMESIZE] = '\0';
	return 0;
}

int remove_folder(struct folder *folder, const char *fol_type)
{
	int _idx_fd = idx_fd;
	int _txt_fd = txt_fd;

	if (fol_type != FOL_LIVE) {
		_idx_fd = openat(idx_fd, fol_type, O_RDONLY);
		if (_idx_fd < 0 && errno != EEXIST) {
			fprintf(stderr, FOL_IDX_DIR"/%s: %s\n", fol_type, strerror(errno));
			return 1;
		}
		_txt_fd = openat(txt_fd, fol_type, O_RDONLY);
		if (_txt_fd < 0 && errno != EEXIST) {
			fprintf(stderr, FOL_TXT_DIR"/%s: %s\n", fol_type, strerror(errno));
			close(_idx_fd);
			return 1;
		}
	}
	unlinkat(_idx_fd, folder->name, 0);
	unlinkat(_txt_fd, folder->name, 0);
	return 0;
}

int move_folder(struct folder *folder, const char *from, const char *to)
{
	int idx_from_fd = idx_fd;
	int idx_to_fd = idx_fd;
	int txt_from_fd = txt_fd;
	int txt_to_fd = txt_fd;
	int ret = 1;

	if (from != FOL_LIVE) {
		idx_from_fd = openat(idx_fd, from, O_RDONLY);
		if (idx_from_fd < 0) {
			fprintf(stderr, FOL_IDX_DIR"/%s: %s\n", from, strerror(errno));
			return 1;
		}
		txt_from_fd = openat(txt_fd, from, O_RDONLY);
		if (txt_from_fd < 0) {
			fprintf(stderr, FOL_TXT_DIR"/%s: %s\n", from, strerror(errno));
			goto close_from_idx;
		}
	}
	if (to != FOL_LIVE) {
		idx_to_fd = openat(idx_fd, to, O_RDONLY);
		if (idx_to_fd < 0) {
			fprintf(stderr, FOL_IDX_DIR"/%s: %s\n", to, strerror(errno));
			goto close_from_txt;
		}
		txt_to_fd = openat(txt_fd, to, O_RDONLY);
		if (txt_to_fd < 0) {
			fprintf(stderr, FOL_TXT_DIR"/%s: %s\n", to, strerror(errno));
			goto close_to_idx;
		}
	}
	ret = renameat(idx_from_fd, folder->name, idx_to_fd, folder->name);
	if (ret != 0 && errno != ENOENT) {
		perror("Failed to move folder index file");
		goto close_to_txt;
	}
	fsync(idx_from_fd);
	ret = renameat(txt_from_fd, folder->name, txt_to_fd, folder->name);
	if (ret == 0)
		fsync(txt_from_fd);
	else if (errno != ENOENT)
		perror("Failed to move folder text file");
close_to_txt:
	if (txt_to_fd != txt_fd)
		close(txt_to_fd);
close_to_idx:
	if (idx_to_fd != idx_fd)
		close(idx_to_fd);
close_from_txt:
	if (txt_from_fd != txt_fd)
		close(txt_from_fd);
close_from_idx:
	if (idx_from_fd != idx_fd)
		close(idx_from_fd);
	return ret;
}
