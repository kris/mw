#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "talker_privs.h"

char *display_cmflags(unsigned long cm)
{
	static char s[40];

	s[0]=0;
	if (cm & CM_ONCHAT) strcat(s,"c");
	if (cm & CM_GLOBAL) strcat(s,"o");
	if (cm & CM_PROTECTED) strcat(s,"p");
	if (cm & CM_FROZEN) strcat(s,"f");
	if (cm & CM_SPY) strcat(s,"y");
	if (cm & CM_VERBOSE) strcat(s,"v");
	if (cm & CM_STICKY) strcat(s,"k");
	if (cm & CM_GAG1) strcat(s,"1");
	if (cm & CM_GAG2) strcat(s,"2");
	if (cm & CM_GAG3) strcat(s,"3");
	if (cm & CM_GAG4) strcat(s,"4");
	if (cm & CM_GAG5) strcat(s,"5");
	if (cm & CM_GAG6) strcat(s,"6");
	return (s);
}

void show_chatmodes(unsigned long cm, char *tmp, int flag)
{
	int i=0;

	if (cm & CM_ONCHAT) tmp[i++]='c'; else if (flag) tmp[i++]='-';
	if (cm & CM_GLOBAL) tmp[i++]='o'; else if (flag) tmp[i++]='-';
	if (cm & CM_PROTECTED) tmp[i++]='p'; else if (flag) tmp[i++]='-';
	if (cm & CM_FROZEN) tmp[i++]='f'; else if (flag) tmp[i++]='-';
	if (cm & CM_SPY) tmp[i++]='y'; else if (flag) tmp[i++]='-';
	if (cm & CM_VERBOSE) tmp[i++]='v'; else if (flag) tmp[i++]='-';
	if (cm & CM_STICKY) tmp[i++]='k'; else if (flag) tmp[i++]='-';
	if (cm & CM_GAG1) tmp[i++]='1'; else if (flag) tmp[i++]='-';
	if (cm & CM_GAG2) tmp[i++]='2'; else if (flag) tmp[i++]='-';
	if (cm & CM_GAG3) tmp[i++]='3'; else if (flag) tmp[i++]='-';
	if (cm & CM_GAG4) tmp[i++]='4'; else if (flag) tmp[i++]='-';
	if (cm & CM_GAG5) tmp[i++]='5'; else if (flag) tmp[i++]='-';
	if (cm & CM_GAG6) tmp[i++]='6'; else if (flag) tmp[i++]='-';
	tmp[i]=0;
}

void show_protection(unsigned long cm, unsigned long cp, char *tmp, int ourapl)
{
	int cpl = (cm & CM_PROTMASK) >> CM_PROTSHIFT;
	int apl = (cp & CP_PROTMASK) >> CP_PROTSHIFT;

/*
	if (cpl > ourapl) cpl = 9;
	if (apl > ourapl) apl = 9;
	snprintf(tmp, 4, "%d/%d", cpl, apl);
	tmp[4]=0;
*/

	if ((cpl <= ourapl) && (apl <= ourapl))
		snprintf(tmp, 4, "%d/%d", cpl, apl);
	else if ((cpl > ourapl) && (apl <= ourapl))
		snprintf(tmp, 4, "?/%d", apl);
	else if ((cpl <= ourapl) && (apl > ourapl))
		snprintf(tmp, 4, "%d/?", cpl);
	else
		snprintf(tmp, 4, "?/?");
}

unsigned long cm_setbycode(unsigned long stat, const char *string)
{
	int mode=0;
	int i;
	unsigned long p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case 'c': p=p|CM_ONCHAT;break;
			case 'o': p=p|CM_GLOBAL;break;
			case 'p': p=p|CM_PROTECTED;break;
			case 'y': p=p|CM_SPY;break;
			case 'v': p=p|CM_VERBOSE;break;
			case 'f': p=p|CM_FROZEN;break;
			case 'k': p=p|CM_STICKY;break;
			case '1': p=p|CM_GAG1;break;
			case '2': p=p|CM_GAG2;break;
			case '3': p=p|CM_GAG3;break;
			case '4': p=p|CM_GAG4;break;
			case '5': p=p|CM_GAG5;break;
			case '6': p=p|CM_GAG6;break;
		};
	if (mode==0) return(p);
	else if (mode==-1) return(stat|p);
	else if (mode==1) return(stat&(~p));
	else return(p);
}


/***************************/
/* user->chatprivs options */

char *display_cpflags(unsigned long cm)
{
	static char s[40];

	s[0]=0;
	if (cm & CP_CANRAW) strcat(s,"R");
	if (cm & CP_CANGAG) strcat(s,"G");
	if (cm & CP_CANZOD) strcat(s,"Z");
	if (cm & CP_CANMROD) strcat(s,"M");
	if (cm & CP_CANGLOBAL) strcat(s,"O");
	if (cm & CP_PROTECT) strcat(s, "P");
	if (cm & CP_FREEZE) strcat(s, "F");
	if (cm & CP_SUMMON) strcat(s, "S");
	if (cm & CP_SPY) strcat(s, "Y");
	if (cm & CP_SCRIPT) strcat(s, "s");
	if (cm & CP_ADVSCR) strcat(s, "A");
	if (cm & CP_DEVEL) strcat(s, "D");
	if (cm & CP_TOPIC) strcat(s, "t");
	return (s);
}

void show_chatprivs(unsigned long cp, char *tmp, int flag)
{
	int i=0;

	if (cp & CP_CANRAW) tmp[i++]='R'; else if (flag) tmp[i++]='-';
	if (cp & CP_CANGAG) tmp[i++]='G'; else if (flag) tmp[i++]='-';
	if (cp & CP_CANZOD) tmp[i++]='Z'; else if (flag) tmp[i++]='-';
	if (cp & CP_CANMROD) tmp[i++]='M'; else if (flag) tmp[i++]='-';
	if (cp & CP_CANGLOBAL) tmp[i++]='O'; else if (flag) tmp[i++]='-';
	if (cp & CP_PROTECT) tmp[i++]='P'; else if (flag) tmp[i++]='-';
	if (cp & CP_FREEZE) tmp[i++]='F'; else if (flag) tmp[i++]='-';
	if (cp & CP_SUMMON) tmp[i++]='S'; else if (flag) tmp[i++]='-';
	if (cp & CP_SPY) tmp[i++]='Y'; else if (flag) tmp[i++]='-';
	if (cp & CP_SCRIPT) tmp[i++]='s'; else if (flag) tmp[i++]='-';
	if (cp & CP_ADVSCR) tmp[i++]='A'; else if (flag) tmp[i++]='-';
	if (cp & CP_DEVEL) tmp[i++]='D'; else if (flag) tmp[i++]='-';
	if (cp & CP_TOPIC) tmp[i++]='t'; else if (flag) tmp[i++]='-';

	tmp[i]=0;
}

unsigned long cp_setbycode(unsigned long stat, const char *string)
{
	int mode=0;
	int i;
	unsigned long p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case 'R': p=p|CP_CANRAW;break;
			case 'G': p=p|CP_CANGAG;break;
			case 'Z': p=p|CP_CANZOD;break;
			case 'M': p=p|CP_CANMROD;break;
			case 'O': p=p|CP_CANGLOBAL;break;
			case 'P': p=p|CP_PROTECT;break;
			case 'F': p=p|CP_FREEZE;break;
			case 'S': p=p|CP_SUMMON;break;
			case 'Y': p=p|CP_SPY;break;
			case 's': p=p|CP_SCRIPT;break;
			case 'A': p=p|CP_ADVSCR;break;
			case 'D': p=p|CP_DEVEL;break;
			case 't': p=p|CP_TOPIC;break;
		};
	if (mode==0) return(p);
	else if (mode==-1) return(stat|p);
	else if (mode==1) return(stat&(~p));
	else return(p);
}
