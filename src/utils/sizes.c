#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>
#include <stddef.h>

#include <mesg.h>
#include "bb.h"
#include "user.h"
#include "files.h"
#include "folders.h"

static void addmap(char *map, const char *name, char key,
		size_t size, size_t offset)
{
	int i;
	printf("-%c %-20s %4lu bytes @%lu\n", key, name, size, offset);
	for (i=offset;i<offset+size;i++)
		map[i]=key;
}

static void showmap(const char *map, unsigned long size)
{
	int i;
	printf("memmap: ");
	for (i=0;i<size;i++) {
		if (map[i]==0)
			printf(".");
		else
			printf("%c", map[i]);
		if (i % 64 == 63 && i<size-1) printf("\nmemmap: ");
	}
	printf("\n");
}

int main(int argc, char ** argv)
{
	struct folder ff;
	struct Header hh;
	struct person pp;
	char *map;

	printf("struct person = %lu bytes\n",sizeof(struct person));
	map = malloc(sizeof(struct person));
	memset(map, 0, sizeof(struct person));
	addmap(map, "name",  	'N', sizeof(pp.name), offsetof(struct person, name));
	addmap(map, "passwd",  	'P', sizeof(pp.passwd), offsetof(struct person, passwd));
	addmap(map, "lastlogout",  'l', sizeof(pp.lastlogout), offsetof(struct person, lastlogout));
	addmap(map, "folders",  'f', sizeof(pp.folders), offsetof(struct person, folders));
	addmap(map, "status",  	's', sizeof(pp.status), offsetof(struct person, status));
	addmap(map, "special",  'S', sizeof(pp.special), offsetof(struct person, special));
	addmap(map, "lastread", 'L', sizeof(pp.lastread), offsetof(struct person, lastread));
	addmap(map, "realname", 'R', sizeof(pp.realname), offsetof(struct person, realname));
	addmap(map, "contact",  'C', sizeof(pp.contact), offsetof(struct person, contact));
	addmap(map, "timeused", 't', sizeof(pp.timeused), offsetof(struct person, timeused));
	addmap(map, "idletime", 'i', sizeof(pp.idletime), offsetof(struct person, idletime));
	addmap(map, "groups",   'g', sizeof(pp.groups), offsetof(struct person, groups));
	addmap(map, "doing",    'd', sizeof(pp.doing), offsetof(struct person, doing));
	addmap(map, "dowhen",   'w', sizeof(pp.dowhen), offsetof(struct person, dowhen));
	addmap(map, "timeout",  'T', sizeof(pp.timeout), offsetof(struct person, timeout));
	addmap(map, "spare",    '_', sizeof(pp.spare), offsetof(struct person, spare));
	addmap(map, "colour",   'c', sizeof(pp.colour), offsetof(struct person, colour));
	addmap(map, "room",     'r', sizeof(pp.room), offsetof(struct person, room));
	addmap(map, "chatprivs", 'C', sizeof(pp.chatprivs), offsetof(struct person, chatprivs));
	addmap(map, "chatmode", 'm', sizeof(pp.chatmode), offsetof(struct person, chatmode));
	showmap(map,sizeof(struct person));
	printf("\n");
	free(map);

	printf("struct header = %lu bytes\n",sizeof(struct Header));

	map = malloc(sizeof(struct Header));
	memset(map, 0, sizeof(struct Header));
	addmap(map, "Ref",  'R', sizeof(hh.Ref), offsetof(struct Header, Ref));
	addmap(map, "date", 'd', sizeof(hh.date), offsetof(struct Header, date));
	addmap(map, "from", 'f', sizeof(hh.from), offsetof(struct Header, from));
	addmap(map, "to",   't', sizeof(hh.to), offsetof(struct Header, to));
	addmap(map, "subject",'S', sizeof(hh.subject), offsetof(struct Header, subject));
	addmap(map, "datafield",'D', sizeof(hh.datafield), offsetof(struct Header, datafield));
	addmap(map, "size",   'z', sizeof(hh.size), offsetof(struct Header, size));
	addmap(map, "status", 's', sizeof(hh.status), offsetof(struct Header, status));
	addmap(map, "replyto", 'r', sizeof(hh.replyto), offsetof(struct Header, replyto));
	addmap(map, "spare", '_', sizeof(hh.spare), offsetof(struct Header, spare));
	showmap(map,sizeof(struct Header));
	printf("\n");
	free(map);


	printf("struct folder = %lu bytes\n",sizeof(struct folder));
	map = malloc(sizeof(struct folder));
	memset(map, 0, sizeof(struct folder));
	addmap(map, "status",  's', sizeof(ff.status), offsetof(struct folder, status));
	addmap(map, "name",  'n', sizeof(ff.name), offsetof(struct folder, name));
	addmap(map, "topic",  't', sizeof(ff.topic), offsetof(struct folder, topic));
	addmap(map, "first",  'f', sizeof(ff.first), offsetof(struct folder, first));
	addmap(map, "last",  'l', sizeof(ff.last), offsetof(struct folder, last));
	addmap(map, "g_status",  'g', sizeof(ff.g_status), offsetof(struct folder, g_status));
	addmap(map, "groups",  'r', sizeof(ff.groups), offsetof(struct folder, groups));
	addmap(map, "spare",  '_', sizeof(ff.spare), offsetof(struct folder, spare));
	showmap(map,sizeof(struct folder));
	printf("\n");
	free(map);

	return 0;
}

