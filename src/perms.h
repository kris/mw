#ifndef PERMS_H
#define PERMS_H

#include "folders.h"
#include "user.h"

char user_stats(const char *string, char stat);
char mesg_stats(char *string, char stat);

void show_user_stats(unsigned char stat, char *tmp, int flag);
void show_mesg_stats(unsigned char stat, char *tmp, int flag);
void show_fold_groups(char stat, char *tmp, int flag);
void show_fold_stats(char stat, char *tmp, int flag);

int allowed_r(struct folder *fol, struct user *usr);
int allowed_w(struct folder *fol, struct user *usr);

int is_private(struct folder *fol, struct user *usr);
int is_moderated(struct folder *fol, struct user *usr);

char folder_stats(char *string, char stat);
char folder_groups(const char *string, char stat);

void set_subscribe(struct user *user, int folder, int status);
int get_subscribe(struct user *user, int folder);

#endif
