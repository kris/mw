#ifndef SOCKET_H
#define SOCKET_H

#include <stdint.h>
#include <jansson.h>
#include "list.h"

#define IPCHOST_DEFAULT	"localhost"
#define IPCPORT_DEFAULT	"9999"

/* which userposn is the System user,
 * 0 is possible but 1 is an impossible location
 */
#define SYSTEM_USER 1

enum ipcsock_state {
	IPCSTATE_CONNECTED,  // connected but not running
	IPCSTATE_VALID,	     // running
	IPCSTATE_ERROR,	     // error, about to close
	IPCSTATE_PURGE,      // soft close
	IPCSTATE_DELETED     // already closed and gone
};

/* packed struct for socket ipc */
typedef struct {
	uint32_t src;		// sending userid
	uint32_t dst;		// destination user/room
	uint32_t type;		// message type
	uint32_t len;		// bytes of body
	uint64_t serial;	// sequence number
	 int64_t when;		// unixtime of sending
} __attribute__((packed)) ipc_msghead_t;

typedef struct {
	ipc_msghead_t		head;
	char *			body;
	int			bodylen;
	int			refcount;
} ipc_message_t;

typedef struct {
	struct list_head 	list;	/* list of connections */
	uint32_t		addr;	/* (p)id of client at other end */
	uint32_t		user;	/* user id of client */
	enum ipcsock_state	state;

	int 			fd;
	char 			p_buffer[4096]; /* incoming data buffer */
	int 			i_buffer;

	ipc_message_t *		incoming; /* partial incoming message */

	struct list_head	outq;	/* outgoing message queue */

} ipc_connection_t;

/* use this to queue a message for transmission */
typedef struct {
	struct list_head list;

	ipc_message_t * msg;

	int headsent;
	int used;
} outq_msg_t;

#define _MIN(a,b) (a<b)?a:b

/* socket.c */
ipc_message_t *ipcmsg_create(uint32_t type,uint32_t src);
void ipcmsg_append(ipc_message_t *msg, const void *data, int len);
void ipcmsg_destination(ipc_message_t *msg, uint32_t dest);
int msg_queue(ipc_message_t *msg, ipc_connection_t *conn);
void ipcmsg_destroy(ipc_message_t *msg);
void ipcmsg_send(ipc_message_t *msg, ipc_connection_t *conn);
void ipcconn_bad(ipc_connection_t *conn);
ipc_connection_t *ipcconn_create(void);
int ipcconn_connect(const char *host, const char *port);
ipc_message_t *read_socket(ipc_connection_t *conn, int doread);
json_t *ipcmsg_json_decode(ipc_message_t *msg);
int ipcmsg_json_encode(ipc_message_t *msg, json_t *js);
json_t *json_init(ipc_message_t *msg);
int json_vaddstring(json_t *js, const char *key, const char *value, ...);
int json_addstring(json_t *js, const char *key, const char *value);
int json_addint(json_t *js, const char *key, int value);
int json_addobject(json_t *js, const char *key, json_t * obj);
const char *json_getstring(json_t *js, const char *key);
int json_getint(json_t *js, const char *key);
void ipcmsg_summary(const char *intro, ipc_message_t *msg);

#ifndef json_array_foreach
#define json_array_foreach(array, index, value) \
	for(index = 0; \
	index < json_array_size(array) && (value = json_array_get(array, index)); \
	index++)
#endif

#endif
