/*********************************************************
 *     The Milliways III System is copyright 1992        *
 *      J.S.Mitchell. (arthur@sugalaxy.swan.ac.uk)       *
 *       see licence for furthur information.            *
 *********************************************************/

#include <string.h>
#include "special.h"

/* short = 16 bits */
unsigned short set_special(const char *string, unsigned short st)
{
	int mode=0;
	int i;
	unsigned short p=0; /*pattern buffer */
	for (i=0;i<strlen(string);i++)
	{
		switch (string[i])
		{
			case '=': mode=0;break;
			case '+': mode=-1;break;
			case '-': mode=1;break;
			case 'C': p=p|(1<<0);break; /* cannot use tell */
			case 'S': p=p|(1<<1);break; /* su toggle */
			case 'W': p=p|(1<<2);break; /* wiz enabled */
			case 'n': p=p|(1<<3);break; /* wizchat off */
			case 'T': p=p|(1<<4);break; /* setable timeout */
			case 't': p=p|(1<<5);break; /* time on wiz mesg */
			case 'P': p=p|(1<<6);break; /* posting info */
			case 'c': p=p|(1<<8);break; /* colour disabled */
			case 'G': p=p|(1<<9);break; /* changeinfo on */
			case 'Q': p=p|(1<<11);break;/* allow quiet mode */
			case 'U': p=p|(1<<12);break;/* username tab-complete */
			case 'D': p=p|(1<<13);break;/* can run devel version */
			case 'o': p=p|(1<<14);break;/* cannot change contact */
			case 'L': p=p|(1<<15);break;/* suppress line wrapping */
		};
	}
	if (mode==0) return(p);
	else if (mode==-1) return(st|p);
	else if (mode==1) return(st&(~p));
	else return(p);
}

void show_special(unsigned short stat, char *tmp, int flag)
{
	int i=0;

	if (stat&(1<<0)) tmp[i++]='C'; else if (flag) tmp[i++]='-';
	if (stat&(1<<1)) tmp[i++]='S'; else if (flag) tmp[i++]='-';
	if (stat&(1<<2)) tmp[i++]='W'; else if (flag) tmp[i++]='-';
	if (stat&(1<<3)) tmp[i++]='n'; else if (flag) tmp[i++]='-';
	if (stat&(1<<4)) tmp[i++]='T'; else if (flag) tmp[i++]='-';
	if (stat&(1<<5)) tmp[i++]='t'; else if (flag) tmp[i++]='-';
	if (stat&(1<<6)) tmp[i++]='P'; else if (flag) tmp[i++]='-';
	if (stat&(1<<8)) tmp[i++]='c'; else if (flag) tmp[i++]='-';
	if (stat&(1<<9)) tmp[i++]='G'; else if (flag) tmp[i++]='-';
	if (stat&(1<<11)) tmp[i++]='Q'; else if (flag) tmp[i++]='-';
	if (stat&(1<<12)) tmp[i++]='U'; else if (flag) tmp[i++]='-';
	if (stat&(1<<13)) tmp[i++]='D'; else if (flag) tmp[i++]='-';
	if (stat&(1<<14)) tmp[i++]='o'; else if (flag) tmp[i++]='-';
	if (stat&(1<<15)) tmp[i++]='L'; else if (flag) tmp[i++]='-';

	tmp[i]=0;
}
