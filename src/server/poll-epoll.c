#include <sys/epoll.h>
#include <string.h>

#include "poll.h"

struct epoll_priv {
	int pollfd;
};

struct epoll_priv ep = {
	.pollfd = -1
};

int poll_addconn(int fd, void *data)
{
	struct epoll_event ev;
	int ret;

	memset(&ev, 0, sizeof(ev));
	ev.events = EPOLLIN | EPOLLERR;
	ev.data.ptr = data;
	ret = epoll_ctl(ep.pollfd, EPOLL_CTL_ADD, fd, &ev);
	return ret;
}

int poll_init(void)
{
	if (ep.pollfd == -1)
		ep.pollfd = epoll_create(30);
	return 0;
}

void poll_delete(int fd)
{
	struct epoll_event ev;

	memset(&ev, 0, sizeof(ev));
	epoll_ctl(ep.pollfd, EPOLL_CTL_DEL, fd, &ev);
}

int poll_with_writes(int fd, void *data)
{
	struct epoll_event ev;

	memset(&ev, 0, sizeof(ev));
	ev.events = EPOLLIN | EPOLLOUT | EPOLLERR;
	ev.data.ptr = data;
	return epoll_ctl(ep.pollfd, EPOLL_CTL_MOD, fd, &ev);
}

int poll_without_writes(int fd, void *data)
{
	int op = (data == NULL) ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
	struct epoll_event ev;

	memset(&ev, 0, sizeof(ev));
	ev.events = EPOLLIN | EPOLLERR;
	ev.data.ptr = data;
	return epoll_ctl(ep.pollfd, op, fd, &ev);
}

int poll_wait(int timeout_millis, int (*callback)(poll_event_t *, int, void *), void *data)
{
#define NEVENTS (20)
	struct epoll_event evs[NEVENTS];
	poll_event_t pevs[NEVENTS];
	int ret;

	ret = epoll_wait(ep.pollfd, evs, NEVENTS, timeout_millis);
	if (ret < 0)
		return ret;
	for (int i = 0; i < ret; i++) {
		memset(&pevs[i], 0, sizeof(pevs[i]));
		pevs[i].data = evs[i].data.ptr;
		pevs[i].is_error = (evs[i].events & (EPOLLERR | EPOLLHUP | EPOLLRDHUP));
		pevs[i].is_read = (evs[i].events & (EPOLLIN | EPOLLPRI));
		pevs[i].is_write = (evs[i].events & (EPOLLOUT));
	}
	ret = callback(pevs, ret, data);
	return ret;
}
