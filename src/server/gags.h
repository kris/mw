#ifndef GAGS_H
#define GAGS_H
#include "user.h"

typedef struct {
	const char *from;
	const char *to;
} gag_pattern_t;

typedef struct {
        const char *name;               /* gag filter name */
        const char *text;               /* gag realname */
        const char *untext;             /* ungag realname */
        const char *gag;                /* text on gag */
        const char *ungag;              /* text on ungag */
        void (*Function)(char *);       /* gag function to call */
} GagInfo;

extern GagInfo gaglist[];

char * apply_gag(struct user *speaker, const char *text);
GagInfo *gag_find(int num);
int gag_code(const char *str);
const char *gag_type(int type);
const char *gag_gag_msg(int type);
const char *gag_ungag_msg(int type);

#endif /* GAGS_H */
