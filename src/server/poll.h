#ifndef SERVER_POLL_H
#define SERVER_POLL_H

typedef struct {
	int is_read;
	int is_write;
	int is_error;
	void *data;
} poll_event_t;

extern int poll_addconn(int fd, void *data);
extern int poll_init(void);
extern void poll_delete(int fd);
extern int poll_with_writes(int fd, void *data);
extern int poll_without_writes(int fd, void *data);
extern int poll_wait(int timeout_millis, int (*callback)(poll_event_t *events, int nmemb, void *data), void *data);

#endif /* SERVER_POLL_H */
