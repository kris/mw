#ifndef SERVSOCK_H
#define SERVSOCK_H
#include <user.h>

typedef enum {
	PERM_WIZCHAT,
	PERM_CHANGEINFO
} perm_t;


/* servsock.c */
int open_mainsock(uint16_t port);
ipc_connection_t *add_connection(int fd);
void accept_connection(int mainsock);
void drop_connection(ipc_connection_t *conn);
void watch_mainsock(int mainsock);
void write_socket(ipc_connection_t *conn);
void process_msg(ipc_connection_t *conn, ipc_message_t *msg);
void msg_attach_to_all(ipc_message_t *msg);
int msg_attach_to_userid(ipc_message_t *msg, uint32_t userposn);
int msg_attach_to_username(ipc_message_t *msg, const char *username);
void msg_attach_to_channel(ipc_message_t *msg, int channel, const char * exclude);
void msg_attach_to_perm(ipc_message_t *msg, perm_t perm);
void msg_attach(ipc_message_t *msg, ipc_connection_t *conn);
int init_server(void);
int init_server_after_setuid(void);
void send_error(ipc_connection_t *conn, ipc_message_t *orig, const char *format, ...);
void msg_apply_gag(struct user * from, ipc_message_t * msg, const char * field);
ipc_message_t * msg_wholist(void);
int32_t who_find(const char * username);
ipc_message_t * msg_error(const char *type);

#endif /* SERVSOCK_H */
