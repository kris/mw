Milliways client-to-server protocol definition
==============================================

Every message sent has the following header :

/* from socket.h */
struct {
        uint32_t src;           // sending userid
        uint32_t dst;           // destination user/room
        uint32_t type;          // message type, ipc.h
        uint32_t len;           // bytes of body
        uint64_t serial;        // sequence number
         int64_t when;          // unixtime of sending
} ipc_msghead_t;

Followed by a body depending on type.

IPC_* numbered items are old format, raw text or instructions.
IPC_* codes using FCC are json formatted messages.

IPC_HELLO - Login to the server, start a session
	Body: (binary)
		pid - 4 bytes - Process / Session ID
		nonce - to end - unique version string for server
	Response:
		none
IPC_UPTIME - Enquire how long the server has been up
	Body: 
		none
	Response: (json)
		uptime - time in seconds
		version - server version string
		
IPC_SAYTOROOM - user said something to the room
	Header:
		dest is room to send to
	Body: (json)
		exclude - username to not send to
		type - sub-type of message
			say - normal talking
			raw - use of .raw command
			emote - use of .emote command
			notsayto - .notsayto command
			says - .say command
			whispers - .whi command
		plural - sub-type of emote
			1 - emote's
			2 - emote'
			3 - emote'd
			4 - emote'll
		text - what was actually said
	Response:
		none

IPC_SAYTOUSER - send text to one specific person
	Header:
		dest is ignored
	Body: (json)
		target - username of person it is for
		same as IPC_SAYTOROOM 
	Response:
		error if target is not logged in

IPC_SAYTOALL - send text to every user 
	Header:
		dest is ignored

IPC_TALKERROR - an error response (usually from server)
	Header:
		dest - PID / session id of offender
	Body: (json)
		type - type of original message that caused error
		text - error message
	Response:
		this is the response

IPC_REPLAY - request retransmission of past messages
	Header:
		dest ignored
	Body: (json)
		In order of preference:-
		serial - show all AFTER this sequence number
		since - unixtime to show since	
		count - now the last $n messages	
	Response:
		you will get all of the relevant messages sent again

IPC_EVENT - an event occured
	Header:
		room number to be told, -1 for all
	Body: (json)
		type 	- type of event that occured
		target 	- who did the event happen to
		success=0|1 - did it work
		text 	- prefered text for announcement
		verbose - additional (amusing) texts
		reason 	- justification (if any) the attacker gave

IPC_ACTION - user requests an action
	Body: (json)
		type - type of event that occured
		target - (optional) who to apply it to
		reason - (optional) reason why you did this
	List of types, and any additional fields
		zod   - Zebedee of Death
		mrod  - Magic Roundabout of Death
		gag   - Apply gagging filter to user
			gag - name of gag to Apply
		ungag - remove gagging filters
		channel - enter/leave a channel
			channel - channel to move to
			target - (opt) if this is a summons/force
	Response:
		type - command request
		target - victim
		success=0|1
		text - prefered acknowledgement text

IPC_WHOLIST - list of all the users currently known to the system
	Send an empty body one of these to the server to request a refresh
	Respose:  json array() each element being one session
		pid	 - session id
		id	 - user id
		name     - username
		realname - users actual name (if permitted)
		idletime - idle time
		doing    - doing info
		dowhen   - when doing last updated
		channel  - Current (primary?) channel
		perms    - object of permissions
		    status
		    chatmode
		    chatprivs
		    protection
		    wizchat


all other codes are old unformatted messages and are to be phased out,
most commonly used is IPC_TEXT which is raw text to be displayed
