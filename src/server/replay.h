#ifndef REPLAY_H
#define REPLAY_H

int replay_init(void);
void store_message(ipc_message_t *msg);
void assign_serial(ipc_message_t *msg);
void replay(ipc_connection_t *conn, ipc_message_t *msg);

#endif /* REPLAY_H */
