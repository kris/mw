#ifndef STR_UTIL_H
#define STR_UTIL_H

#ifdef __clang__
#define mw_printf_func(stridx, vastart) __attribute__((format(printf,stridx,vastart)))
#elif defined __GNUC__
#define mw_printf_func(stridx, vastart) __attribute__((format(gnu_printf,stridx,vastart)))
#else
#define mw_printf_func(stridx, vastart)
#endif

int stringcmp(const char *a, const char *b, int n);
void strip_str(char *string);
int get_rand(int min, int max);
void string_add(char **str, const char *fmt, ...) mw_printf_func(2, 3);
int allspace(char *in);
void strlower(char *szString);
void escprintf(const char *szFormat, ...);
char *strip_colours(const char *text);
char *quotetext(const char *a);

#endif /* STR_UTIL_H */
