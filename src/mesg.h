#ifndef MW_MESG_H
#define MW_MESG_H

#include <user.h>

#define SUBJECTSIZE 40

struct Header {
	int32_t Ref;
	int32_t date;
	char from[NAMESIZE+1];
	char to[SUBJECTSIZE+1];
	char subject[SUBJECTSIZE+1];
	int32_t datafield;
	int32_t size;
	char status;
	int32_t replyto;
	char spare[4]; /*adjust size as necessary */
};

/*	Header.status
bit	use
0	moderator approved
1	marked for deletion
2	has a reply
3	is a remote posting
4	-
5	-
6	-
7	-
*/

#endif /* MW_MESG_H */
